Magento
*******

Django Magento

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-magento
  # or
  python3 -m venv venv-magento

  source venv-magento/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
