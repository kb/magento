# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.core.management.base import BaseCommand

from magento.models import MagentoSettings
from stock.models import Product, ProductType, ProductCategory


class Command(BaseCommand):

    help = "Magento demo data"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        product_type = ProductType.objects.init_product_type(
            "default", "Default"
        )
        product_category = ProductCategory.objects.init_product_category(
            "default", "Default", product_type
        )
        missing_product = Product.objects.init_product(
            "missing", "Missing Lines", "", Decimal("0.01"), product_category
        )
        shipping_product = Product.objects.init_product(
            "shipping", "Shipping", "", Decimal("0.01"), product_category
        )
        magento_settings = MagentoSettings.load()
        magento_settings.missing_product = missing_product
        magento_settings.shipping_product = shipping_product
        magento_settings.save()
        self.stdout.write("{} - Complete".format(self.help))
