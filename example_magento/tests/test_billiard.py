# -*- encoding: utf-8 -*-
# import io
#
# from billiard import reduction
# from pickle import Pickler
#
# from magento.models import MagentoError
#
#
# def test_forking_pickler():
#     """Trying (and failing) to reproduce the 'PicklingError'.
#
#     billiard.pool.MaybeEncodingError:
#     Error sending result:''
#     (1, <ExceptionInfo: MagentoError()>, None)''.
#     Reason:
#     ''PicklingError("Can't pickle <class 'magento.models.MagentoError'>:
#     it's not the same object as magento.models.MagentoError",)''.
#
#     https://www.kbsoftware.co.uk/crm/ticket/4432/
#
#     """
#     magento_error = MagentoError("Apples")
#     # pickle
#     buf = io.BytesIO()
#     Pickler(buf).dump(magento_error)
#     # reduction
#     reduction.ForkingPickler.dumps(magento_error)
