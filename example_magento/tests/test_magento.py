# -*- encoding: utf-8 -*-
import json
import pytest

from unittest import mock

from magento.service import MagentoClient
from magento.tests.factories import MagentoSiteFactory


def _invoice(data):
    file_name = "invoice.json"
    _write_to_file(file_name, data)


def _sales_order(data):
    file_name = "sales_order.json"
    _write_to_file(file_name, data)


def _write_to_file(file_name, data):
    with open(file_name, "w") as f:
        f.write(json.dumps(data, indent=4))


@pytest.mark.django_db
def test_invoice():
    magento = MagentoClient(MagentoSiteFactory())
    with mock.patch("magento.service.MagentoClient.invoice") as m:
        m.return_value = {"increment_id": "abc", "grand_total": 10}
        invoice = magento.invoice("100018249")
    _invoice(invoice)


@pytest.mark.django_db
def test_sales_order():
    magento = MagentoClient(MagentoSiteFactory())
    with mock.patch("magento.service.MagentoClient.sales_order") as m:
        m.return_value = {
            "customer_email": "code@pkimber.net",
            "grand_total": 10,
            "increment_id": "abc",
        }
        data = magento.sales_order("100019985")
    _sales_order(data)
