# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import TemplateView

from base.view_utils import BaseMixin


class DashView(
    BaseMixin, LoginRequiredMixin, StaffuserRequiredMixin, TemplateView
):
    template_name = "example/dash.html"


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class SettingsView(
    BaseMixin, LoginRequiredMixin, StaffuserRequiredMixin, TemplateView
):
    template_name = "example/settings.html"
