# -*- encoding: utf-8 -*-
from django import forms

from .models import (
    MagentoSettings,
    MagentoSite,
    MagentoSiteCustomerGroup,
    MagentoSitePaymentMethod,
)


class MagentoSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("missing_product", "shipping_product"):
            f = self.fields[name]
            f.required = False
            f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = MagentoSettings
        fields = ("missing_product", "shipping_product")

    def clean(self):
        cleaned_data = super().clean()
        missing_product = cleaned_data.get("missing_product")
        shipping_product = cleaned_data.get("shipping_product")
        if missing_product and shipping_product:
            pass
        else:
            raise forms.ValidationError(
                "Please select a product for shipping and a "
                "product for transactions with missing lines",
                code="magento_settings__product",
            )
        return cleaned_data


class MagentoSiteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("description", "api_url"):
            self.fields[name].widget.attrs.update({"class": "pure-input-1"})
        f = self.fields["on_account_customer_groups"]
        f.queryset = MagentoSiteCustomerGroup.objects.current(self.instance)
        f.required = False
        f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = MagentoSite
        fields = (
            "description",
            "api_url",
            "username",
            "password",
            "on_account_customer_groups",
        )


class MagentoSitePaymentMethodForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["payment_processor"]
        f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = MagentoSitePaymentMethod
        fields = ("on_account", "payment_processor")
