# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from magento.models import download_invoice, MagentoSite
from magento.service import MagentoClient


class Command(BaseCommand):

    help = "Download Magento invoice"

    def add_arguments(self, parser):
        parser.add_argument("increment_id", nargs="+", type=int)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        if 1 == MagentoSite.objects.count():
            magento_site = MagentoSite.objects.first()
            magento = MagentoClient(magento_site)
            self.stdout.write("- Using {}".format(magento_site.api_url))
            for increment_id in options["increment_id"]:
                self.stdout.write(
                    "- Downloading invoice '{}'".format(increment_id)
                )
                download_invoice(magento, increment_id)
        else:
            raise CommandError(
                "This command can only work with a single Magento site in "
                "*Settings* (we have {}).".format(MagentoSite.objects.count())
            )
        self.stdout.write("{} - Complete".format(self.help))
