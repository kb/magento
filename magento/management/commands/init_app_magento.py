# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from magento.models import SYSTEM_GENERATED_USER_NAME


class Command(BaseCommand):

    help = "Initialise Magento app"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        user_model_class = get_user_model()
        try:
            user = user_model_class.objects.get(
                username=SYSTEM_GENERATED_USER_NAME
            )
            self.stdout.write("'{}' user already exists.".format(user.username))
        except get_user_model().DoesNotExist:
            user = user_model_class(username=SYSTEM_GENERATED_USER_NAME)
            user.save()
            self.stdout.write("Created '{}' user.".format(user.username))
        self.stdout.write("{} - Complete".format(self.help))
