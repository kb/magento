# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from invoice.models import Invoice
from magento.models import MagentoInvoice, MagentoSite, MagentoSitePaymentMethod
from magento.service import MagentoClient


class Command(BaseCommand):

    FORMAT = "  {:>35}: {}"
    help = "Check payment method for Magento invoice"

    def add_arguments(self, parser):
        parser.add_argument("invoice_number", nargs="+", type=int)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        magento_site = MagentoSite.objects.first()
        for invoice_number in options["invoice_number"]:
            invoice = Invoice.objects.get(number=invoice_number)
            self.stdout.write(
                self.FORMAT.format("Invoice", invoice.invoice_number)
            )
            self.stdout.write(
                self.FORMAT.format("Contact", invoice.contact.full_name)
            )
            self.stdout.write(self.FORMAT.format("Date:", invoice.invoice_date))
            magento_invoice = MagentoInvoice.objects.get(invoice=invoice)
            self.stdout.write(
                self.FORMAT.format(
                    "Magento Order Increment",
                    magento_invoice.magento_order_increment_id,
                )
            )
            self.stdout.write(
                self.FORMAT.format(
                    "Magento Order ID", magento_invoice.magento_order_id
                )
            )
            magento = MagentoClient(magento_site)
            data = magento.sales_order(
                {"increment_id": magento_invoice.magento_order_increment_id}
            )
            payment_method = data["payment"]["method"]
            self.stdout.write(
                self.FORMAT.format("Magento Payment Method", payment_method)
            )
            self.stdout.write(
                self.FORMAT.format("Magento Total Paid", data["total_paid"])
            )
            self.stdout.write(
                self.FORMAT.format(
                    "Magento Total Invoiced", data["total_invoiced"]
                )
            )
            magento_site_payment_method = MagentoSitePaymentMethod.objects.get(
                magento_site=magento_site, slug=payment_method
            )
            self.stdout.write(
                self.FORMAT.format(
                    "On Account (even if paid in full)",
                    magento_site_payment_method.on_account,
                )
            )
        self.stdout.write("{} - Complete".format(self.help))
