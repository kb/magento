# -*- encoding: utf-8 -*-
import xmlrpc.client

from django.core.management.base import BaseCommand

from magento.models import MagentoClient, MagentoSite


class Command(BaseCommand):

    help = "Login to Magento Sites"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        magento_site_pks = [
            x.pk for x in MagentoSite.objects.current().order_by("pk")
        ]
        count = 0
        for pk in magento_site_pks:
            count = count + 1
            magento_site = MagentoSite.objects.get(pk=pk)
            self.stdout.write(
                "{}. Login to: {}".format(count, magento_site.description)
            )
            self.stdout.write("   {}".format(magento_site.api_url))
            client = MagentoClient(magento_site)
            self.stdout.write("   {}".format(client._api_url()))
            # self.stdout.write("   {}".format(magento_site.username))
            # self.stdout.write("   {}".format(magento_site.password))
            try:
                client.connect()
                self.stdout.write("   Yay... it worked!")
            except xmlrpc.client.ProtocolError as e:
                self.stdout.write("")
                self.stdout.write("   A protocol error occurred :(")
                self.stdout.write("   Error code: {}".format(e.errcode))
                self.stdout.write("   Error message: {}".format(e.errmsg))
                self.stdout.write("   URL: {}".format(e.url))
                for k, v in e.headers.items():
                    self.stdout.write("   {}: {}".format(k, v))

        self.stdout.write("{} - Complete".format(self.help))
