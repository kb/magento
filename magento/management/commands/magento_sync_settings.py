# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from magento.tasks import magento_sync_settings


class Command(BaseCommand):

    help = "Sync Magento Settings (Country Codes etc)"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = magento_sync_settings()
        self.stdout.write("{} ({} records) - Complete".format(self.help, count))
