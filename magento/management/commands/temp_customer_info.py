# -*- encoding: utf-8 -*-
"""

Magento

Identify "account" customers, so we always post their invoices "on-account"

https://www.kbsoftware.co.uk/crm/ticket/4924/

"""

from django.core.management.base import BaseCommand

from magento.models import MagentoSite
from magento.service import MagentoClient


class Command(BaseCommand):

    help = "Temporary - customer information #4924"

    def _customer(self, customer_id):
        for magento_site in MagentoSite.objects.all():
            magento = MagentoClient(magento_site)
            data = magento.customer(customer_id)
            for count, row in enumerate(data):
                print(count, row)
            print(data)

    def _customers(self, customer_group_id):
        for magento_site in MagentoSite.objects.all():
            magento = MagentoClient(magento_site)
            data = magento._api_call(
                "customer.list", [{"group_id": customer_group_id}]
            )
            for count, row in enumerate(data):
                print(count, row)

    def _groups(self):
        for magento_site in MagentoSite.objects.all():
            magento = MagentoClient(magento_site)
            data = magento._api_call("customer_group.list")
            for row in data:
                print(row)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        self._groups()
        self._customers(10)
        self._customer(36241)
        self.stdout.write("{} - Complete".format(self.help))
