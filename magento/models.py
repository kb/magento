# -*- encoding: utf-8 -*-
import logging

from datetime import date
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError, transaction, models
from reversion import revisions as reversion

from base.model_utils import TimeStampedModel, TimedCreateModifyDeleteModel
from base.singleton import SingletonModel
from contact.models import Contact, ContactEmail, ContactPhone, ContactAddress
from finance.models import Country, Currency, VatSettings
from invoice.models import (
    Invoice,
    InvoiceContact,
    InvoiceCredit,
    InvoiceIssue,
    InvoiceLine,
    PaymentProcessor,
)
from stock.models import ProductType, ProductCategory, Product
from .service import MagentoClient


CREDIT_NOTE_PREFIX = "C"
INVOICE_PREFIX = "I"
logger = logging.getLogger(__name__)
MISC_PRODUCT_NAME = "Miscellaneous Product"
MISC_PRODUCT_SLUG = "miscellaneous-product"
SYSTEM_GENERATED_USER_NAME = "system.generated"


class MagentoError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


def create_magento_invoice(invoice, magento_invoice_data):
    MagentoInvoice.objects.create_magento_invoice(
        invoice,
        magento_invoice_data.invoice_id or 0,
        magento_invoice_data.invoice_increment_id,
        magento_invoice_data.order.order_id,
        magento_invoice_data.order_increment_id,
        magento_invoice_data.order.customer_id,
    )


def create_or_find_contact(
    user, magento_order_data, magento_customer_data, magento_site
):
    country = MagentoSiteCountry.objects.find_country(
        magento_site, magento_order_data.address.country_id
    )
    # is this an on account customer?
    if magento_customer_data is None:
        on_account = False
    else:
        group = MagentoSiteCustomerGroup.objects.get_by_customer_group_id(
            magento_site, magento_customer_data.group_id
        )
        on_account = magento_site.customer_group_on_account(group)
    try:
        contact = Contact.objects.get(user=user)
    except Contact.DoesNotExist:
        contact = Contact(user=user, company_name=magento_order_data.company)
        contact.save()
        ContactEmail(contact=contact, email=magento_order_data.email).save()
        if magento_order_data.phone:
            ContactPhone(contact=contact, phone=magento_order_data.phone).save()
        ContactAddress(
            contact=contact,
            address_one=magento_order_data.address.address_one[:100],
            address_two=magento_order_data.address.address_two[:100],
            town=magento_order_data.address.town or "",
            country=country.name,
            postal_code=magento_order_data.address.postal_code[-50:],
            admin_area=magento_order_data.address.admin_area,
        ).save()
    # update contact details
    parameters = dict(country=country, on_account=on_account)
    if magento_order_data.vat_number:
        # create / update VAT number
        parameters.update(dict(vat_number=magento_order_data.vat_number))
    InvoiceContact.objects.init_invoice_contact(contact, **parameters)
    return contact


def create_or_find_credit_note(magento_credit_data, contact, invoice_source):
    """Create or find a credit note.

    If a credit memo doesn't exist in the database for a given creditmemo_id,
    one is created.

    """
    try:
        credit_note = Invoice.objects.get(
            number=magento_credit_data.invoice_increment_id,
            prefix=CREDIT_NOTE_PREFIX,
            is_credit=True,
        )
    except Invoice.DoesNotExist:
        currency = create_or_find_currency(magento_credit_data.currency_code)
        with transaction.atomic():
            user = system_generated_user()
            credit_note = Invoice(
                contact=contact,
                currency=currency,
                exchange_rate=magento_credit_data.exchange_rate,
                invoice_date=magento_credit_data.invoice_date,
                is_credit=True,
                is_draft=False,
                number=magento_credit_data.invoice_increment_id,
                prefix=CREDIT_NOTE_PREFIX,
                source=invoice_source or "",
                user=user,
            )
            credit_note.save()
            create_magento_invoice(credit_note, magento_credit_data)
            process_invoice_lines(
                credit_note,
                get_invoice_contact(contact),
                magento_credit_data.lines,
                user,
            )
    return credit_note


def create_or_find_currency(currency_code):
    try:
        currency = Currency.objects.get(slug=currency_code)
    except Currency.DoesNotExist:
        currency = Currency(slug=currency_code)
        currency.save()
    return currency


def create_or_find_invoice(
    magento_site, magento_invoice_data, contact, invoice_source
):
    """Find an existing invoice - or create a new one.

    If an invoice doesn't exist in the database for a given
    ``invoice_increment_id``, one is created.

    """
    try:
        invoice = Invoice.objects.get(
            number=magento_invoice_data.invoice_increment_id,
            prefix=INVOICE_PREFIX,
            is_credit=False,
        )
    except Invoice.DoesNotExist:
        user = system_generated_user()
        with transaction.atomic():
            currency = create_or_find_currency(
                magento_invoice_data.currency_code
            )
            invoice = Invoice(
                contact=contact,
                currency=currency,
                exchange_rate=magento_invoice_data.exchange_rate,
                invoice_date=magento_invoice_data.invoice_date,
                is_credit=False,
                is_draft=False,
                number=magento_invoice_data.invoice_increment_id,
                prefix=INVOICE_PREFIX,
                source=invoice_source,
                user=user,
            )
            invoice_contact = get_invoice_contact(contact)
            if magento_invoice_data.order.paid:
                payment_method = create_or_find_payment_method(
                    magento_site, magento_invoice_data.order
                )
                # if 'on_account', then the invoice is *not* paid
                if payment_method.on_account or invoice_contact.on_account:
                    pass
                else:
                    invoice.upfront_payment = True
                    invoice.upfront_payment_processor = (
                        payment_method.payment_processor
                    )
            invoice.save()
            create_magento_invoice(invoice, magento_invoice_data)
            process_invoice_lines(
                invoice, invoice_contact, magento_invoice_data.lines, user
            )
    return invoice


def create_or_find_payment_method(magento_site, order):
    return MagentoSitePaymentMethod.objects.init_magento_site_payment_method(
        magento_site, order.payment_method, order.payment_method
    )


def create_or_find_product(product_data):
    if product_data.is_missing:
        result = product_for_missing_lines()
    elif product_data.is_shipping:
        result = product_for_shipping()
    else:
        result = init_product(
            product_data.slug,
            product_data.name,
            product_data.description,
            product_data.price,
        )
    return result


def create_or_find_user(email, firstname, lastname):
    try:
        user = User.objects.get(username=email)
    except User.DoesNotExist:
        user = User(
            username=email,
            first_name=firstname,
            last_name=lastname,
            is_active=False,
        )
        user.set_unusable_password()
        user.save()
    return user


def download_credit_note(magento, credit_note_increment_id):
    """Download a Magento credit note."""
    magento_credit_data = magento.credit_memo_data(credit_note_increment_id)
    magento_credit_data.order = magento.sales_order_data(
        magento_credit_data.order_increment_id
    )
    magento_customer_data = magento.customer_data(
        magento_credit_data.order.customer_id
    )
    with transaction.atomic():
        # creates users, contacts and invoices if they don't exist
        user = create_or_find_user(
            magento_credit_data.order.email,
            magento_credit_data.order.first_name,
            magento_credit_data.order.last_name,
        )
        contact = create_or_find_contact(
            user,
            magento_credit_data.order,
            magento_customer_data,
            magento.magento_site,
        )
        credit_note = create_or_find_credit_note(
            magento_credit_data, contact, magento.magento_site.description
        )
        for message in magento_credit_data.create_issue_messages:
            InvoiceIssue.objects.init_invoice_issue(credit_note, message)
        if not credit_note.gross == magento_credit_data.base_grand_total:
            message = (
                "Credit note {}: gross amount ({}) does not equal "
                "Magento total ({})".format(
                    credit_note.invoice_number,
                    credit_note.gross,
                    magento_credit_data.base_grand_total,
                )
            )
            logger.error(message)
            raise MagentoError(message)
        logger.info(
            "Downloaded credit note {} dated {}".format(
                credit_note.invoice_number,
                credit_note.invoice_date.strftime("%d/%m/%Y"),
            )
        )
        if magento_credit_data.invoice_id:
            try:
                # credit notes link to invoices via the ``invoice_id``
                magento_invoice = MagentoInvoice.objects.get(
                    invoice__is_credit=False,
                    magento_invoice_id=magento_credit_data.invoice_id,
                )
                InvoiceCredit.objects.init_invoice_credit(
                    magento_invoice.invoice, credit_note
                )
                logger.info(
                    "Credit note {} linked to invoice {}".format(
                        credit_note.invoice_number,
                        magento_invoice.invoice.invoice_number,
                    )
                )
            except MagentoInvoice.DoesNotExist:
                # Note: If you are testing the system and have not
                # downloaded invoices for earlier dates, then this will be a
                # common issue because the invoice won't have been created.
                InvoiceIssue.objects.init_invoice_issue(
                    credit_note, "Cannot find invoice for credit note"
                )
        else:
            InvoiceIssue.objects.init_invoice_issue(
                credit_note, "Not linked to an invoice"
            )


def download_credit_notes(magento_site, transaction_date):
    count = 0
    magento = MagentoClient(magento_site)
    credit_memos = magento.filter_credit_notes(transaction_date)
    logger.info("Total number of records: {}".format(len(credit_memos)))
    for credit_note in credit_memos:
        credit_note_increment_id = int(credit_note["increment_id"])
        download_credit_note(magento, credit_note_increment_id)
        count = count + 1
    return count


def download_invoice(magento, invoice_increment_id):
    """Download a Magento invoice."""
    magento_invoice_data = magento.invoice_id_data(invoice_increment_id)
    magento_invoice_data.order = magento.sales_order_data(
        magento_invoice_data.order_increment_id
    )
    magento_customer_data = magento.customer_data(
        magento_invoice_data.order.customer_id
    )
    with transaction.atomic():
        # creates users, contacts and invoices if they don't exist
        user = create_or_find_user(
            magento_invoice_data.order.email,
            magento_invoice_data.order.first_name,
            magento_invoice_data.order.last_name,
        )
        contact = create_or_find_contact(
            user,
            magento_invoice_data.order,
            magento_customer_data,
            magento.magento_site,
        )
        invoice = create_or_find_invoice(
            magento.magento_site,
            magento_invoice_data,
            contact,
            magento.magento_site.description,
        )
        for message in magento_invoice_data.create_issue_messages:
            InvoiceIssue.objects.init_invoice_issue(invoice, message)
        if not invoice.gross == magento_invoice_data.base_grand_total:
            message = (
                "Invoice {}: gross amount ({}) does not equal "
                "Magento total ({})".format(
                    invoice.invoice_number,
                    invoice.gross,
                    magento_invoice_data.base_grand_total,
                )
            )
            logger.error(message)
            raise MagentoError(message)
        logger.info(
            "Downloaded invoice {} dated {}".format(
                invoice.invoice_number,
                invoice.invoice_date.strftime("%d/%m/%Y"),
            )
        )


def download_invoices(magento_site, transaction_date):
    """Download invoices for a transaction date."""
    magento = MagentoClient(magento_site)
    invoice_list = magento.filter_invoice(transaction_date)
    logger.info("Total number of records: {}".format(len(invoice_list)))
    count = 0
    for invoice in invoice_list:
        invoice_increment_id = int(invoice["increment_id"])
        download_invoice(magento, invoice_increment_id)
        count = count + 1
    return count


def find_vat_code(invoice_contact, vat_rate):
    """Find the VAT code.

    We use standard rate or exempt:
    https://www.kbsoftware.co.uk/crm/ticket/4392/

    Zero rated EU invoices should use the "Zero Rated EC Goods Income" VAT code:
    https://www.kbsoftware.co.uk/crm/ticket/4422/

    """
    message = "<None>"
    vat_code = None
    vat_settings = VatSettings.load()
    if vat_rate:
        vat_code = vat_settings.standard_vat_code
        message = "Standard"
    else:
        if invoice_contact.is_european_union_vat_transaction():
            vat_code = vat_settings.zero_rate_ec_vat_code
            message = "Zero Rate EU"
        else:
            vat_code = vat_settings.zero_rate_vat_code
            message = "Zero Rate"
    if not vat_code:
        country_code = "<None>"
        if invoice_contact.country:
            country_code = invoice_contact.country.iso2_code
        raise MagentoError(
            "Cannot find a {} VAT code for {} ({}) "
            "(VAT rate {}, Country {}, VAT Number {})".format(
                message,
                invoice_contact.contact.full_name,
                invoice_contact.contact.pk,
                vat_rate,
                country_code,
                invoice_contact.vat_number or "<None>",
            )
        )
    return vat_code


def get_invoice_contact(contact):
    try:
        return contact.invoicecontact
    except InvoiceContact.DoesNotExist:
        raise MagentoError(
            "Contact {} ({}) does not have a 'InvoiceContact' "
            "record".format(contact.full_name, contact.pk)
        )


def init_product(slug, name, description, price):
    try:
        product = Product.objects.get(slug=slug)
    except Product.DoesNotExist:
        product_type = ProductType.objects.init_product_type(
            MISC_PRODUCT_SLUG, MISC_PRODUCT_NAME
        )
        category = ProductCategory.objects.init_product_category(
            MISC_PRODUCT_SLUG, MISC_PRODUCT_NAME, product_type
        )
        product = Product.objects.init_product(
            slug, name, description, price, category
        )
    return product


def process_invoice_lines(invoice, invoice_contact, lines, user):
    """Post a list of ``Line`` objects into actual invoice lines."""
    line_number = 0
    for line in lines:
        line_number = line_number + 1
        # PJK, 12/05/2019:
        # The original discount calculation was also applied to the price.
        # Search for 'create_or_find_invoice_line' in earlier commits.
        product = create_or_find_product(line.product_data)
        vat_code = find_vat_code(invoice_contact, line.vat_rate)
        InvoiceLine(
            invoice=invoice,
            user=user,
            line_number=line_number,
            product=product,
            quantity=line.quantity,
            price=line.price,
            discount=line.discount,
            net=line.net - line.discount,
            vat=line.vat,
            vat_code=vat_code,
            vat_rate=line.vat_rate,
        ).save()


def product_for_missing_lines():
    missing_product = None
    magento_settings = MagentoSettings.load()
    try:
        missing_product = magento_settings.missing_product
    except ObjectDoesNotExist:
        pass
    if not missing_product:
        raise MagentoError(
            "Cannot find the product for a transaction with missing lines"
        )
    return missing_product


def product_for_shipping():
    shipping_product = None
    magento_settings = MagentoSettings.load()
    try:
        shipping_product = magento_settings.shipping_product
    except ObjectDoesNotExist:
        pass
    if not shipping_product:
        raise MagentoError(
            "Cannot find a product for shipping in Magento settings"
        )
    return shipping_product


def sync_magento(transaction_date):
    if transaction_date >= date.today():
        raise MagentoError(
            "Magento invoices can only be downloaded for dates "
            "in the past (i.e. {} must be before {})".format(
                transaction_date.strftime("%d/%m/%Y"),
                date.today().strftime("%d/%m/%Y"),
            )
        )
    count = 0
    magento_site_pks = [x.pk for x in MagentoSite.objects.current()]
    for pk in magento_site_pks:
        magento_site = MagentoSite.objects.get(pk=pk)
        # has the download already been completed for this site?
        if magento_site.is_download_complete(transaction_date):
            pass
        else:
            count = count + download_invoices(magento_site, transaction_date)
            count = count + download_credit_notes(
                magento_site, transaction_date
            )
            magento_site.set_download_complete(transaction_date)
    return count


class SyncSettings:
    def _sync_magento_country_codes(self):
        count = 0
        for magento_site in MagentoSite.objects.all():
            magento = MagentoClient(magento_site)
            data = magento.country_list()
            for row in data:
                country = Country.objects.init_country(
                    row["iso2_code"], row["iso3_code"], row["name"]
                )
                MagentoSiteCountry.objects.init_country(
                    magento_site, row["country_id"], country
                )
                count = count + 1
        return count

    def _sync_magento_customer_groups(self):
        count = 0
        for magento_site in MagentoSite.objects.all():
            magento = MagentoClient(magento_site)
            data = magento.customer_group_list()
            for row in data:
                MagentoSiteCustomerGroup.objects.init_customer_group(
                    magento_site,
                    int(row["customer_group_id"]),
                    row["customer_group_code"],
                )
                count = count + 1
        return count

    def sync(self):
        count = 0
        count = count + self._sync_magento_country_codes()
        count = count + self._sync_magento_customer_groups()
        return count


def system_generated_user():
    return User.objects.get(username=SYSTEM_GENERATED_USER_NAME)


class MagentoInvoiceManager(models.Manager):
    def create_magento_invoice(
        self,
        invoice,
        magento_invoice_id,
        magento_invoice_increment_id,
        magento_order_id,
        magento_order_increment_id,
        magento_customer_id,
    ):
        x = self.model(
            invoice=invoice,
            magento_invoice_id=magento_invoice_id,
            magento_invoice_increment_id=magento_invoice_increment_id,
            magento_order_id=magento_order_id,
            magento_order_increment_id=magento_order_increment_id,
            magento_customer_id=magento_customer_id,
        )
        x.save()
        return x


class MagentoInvoice(TimeStampedModel):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    magento_invoice_id = models.IntegerField()
    magento_invoice_increment_id = models.IntegerField()
    magento_order_id = models.IntegerField(blank=True, null=True)
    magento_order_increment_id = models.IntegerField(blank=True, null=True)
    magento_customer_id = models.IntegerField(blank=True, null=True)
    objects = MagentoInvoiceManager()

    class Meta:
        verbose_name = "Magento Invoice"
        verbose_name_plural = "Magento Invoices"

    def __str__(self):
        return "Invoice {}: Magento Invoice ID: {}".format(
            self.invoice.invoice_number, self.magento_invoice_id
        )


class MagentoSettings(SingletonModel):
    missing_product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        help_text="Product used for missing lines",
        related_name="+",
    )
    shipping_product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        help_text="Product used for shipping costs",
        related_name="+",
    )

    class Meta:
        verbose_name = "Magento settings"

    def __str__(self):
        return "Missing product line '{}', Shipping product '{}'".format(
            self.missing_product.slug, self.shipping_product.slug
        )


reversion.register(MagentoSettings)


class MagentoSiteManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)

    def is_download_complete(self, transaction_date):
        """Check all the sites to see if the downloads are complete.

        .. note:: To check one site, use ``is_download_complete` for the model

        """
        total = self.model.objects.count()
        count = self.model.objects.filter(
            magentositeaudit__date_complete=transaction_date
        ).count()
        return bool(total > 0 and (total == count))

    def is_valid_download(self, transaction_date):
        """Is it OK to download transactions on this date?

        Only allow download if either:

        1. The previous days download succeeded.
        2. This is the first ever download.

        """
        previous_day = transaction_date + relativedelta(days=-1)
        result = self.is_download_complete(previous_day)
        if not result:
            # the first ever download
            result = not MagentoSiteAudit.objects.exclude(
                date_complete=transaction_date
            ).exists()
        return result


class MagentoSite(TimedCreateModifyDeleteModel):
    """A Magento site.

    This is to allow import to one invoice database from one (or more) Magento
    sites.

    """

    api_url = models.URLField()
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    description = models.CharField(
        max_length=100,
        help_text=(
            "Keep this short - it is copied to the 'source' field on the "
            "invoice and may be used for analytics etc."
        ),
    )
    on_account_customer_groups = models.ManyToManyField(
        "MagentoSiteCustomerGroup",
        help_text="On-account customer groups",
    )
    objects = MagentoSiteManager()

    class Meta:
        ordering = ("api_url",)
        verbose_name = "Magento URL"

    def __str__(self):
        return self.api_url

    def audit(self):
        return self.magentositeaudit_set.all()

    def customer_group_on_account(self, customer_group):
        """Is this customer group an 'on-account' posting?"""
        result = False
        if self.pk == customer_group.magento_site.pk:
            pass
        else:
            raise MagentoError(
                "Customer group '{}' is not linked to site '{}' "
                "(it is linked to site '{}')".format(
                    customer_group.pk,
                    self.pk,
                    customer_group.magento_site.pk,
                )
            )
        try:
            magento_customer_group_id = customer_group.magento_customer_group_id
            self.on_account_customer_groups.get(
                magento_customer_group_id=magento_customer_group_id
            )
            # we found a group, so it is *on-account*
            result = True
        except MagentoSiteCustomerGroup.DoesNotExist:
            pass
        return result

    def is_download_complete(self, date_complete):
        """Is the download complete for this site / date?

        .. note:: To check all the sites, use ``is_download_complete` for the
                  model manager.

        """
        result = False
        try:
            self.magentositeaudit_set.get(date_complete=date_complete)
            result = True
        except MagentoSiteAudit.DoesNotExist:
            pass
        return result

    def last_audit(self):
        qs = self.audit().order_by("-date_complete")
        return qs.first()

    def set_download_complete(self, date_complete):
        """Mark the download as complete for this site and date."""
        try:
            MagentoSiteAudit(
                magento_site=self, date_complete=date_complete
            ).save()
        except IntegrityError:
            raise MagentoError(
                "The download for {} site '{}' has already been "
                "completed".format(
                    date_complete.strftime("%d/%m/%Y"), self.description
                )
            )


reversion.register(MagentoSite)


class MagentoSiteAuditManager(models.Manager):
    def set_complete(self, magento_site, date_complete):
        try:
            self.model.objects.get(
                magento_site=magento_site, date_complete=date_complete
            )
            raise MagentoError(
                "The Magento import for {} has already been completed "
                "(site {} {})".format(
                    date_complete.strftime("%d/%m/%Y"),
                    magento_site.pk,
                    magento_site.description,
                )
            )
        except self.model.DoesNotExist:
            self.model(
                magento_site=magento_site, date_complete=date_complete
            ).save()


class MagentoSiteAudit(TimeStampedModel):
    """Audit downloads from a Magento site.

    A ``date_complete`` row will be created when all the imports for that day
    are complete i.e. invoices and credit notes.

    """

    magento_site = models.ForeignKey(MagentoSite, on_delete=models.CASCADE)
    date_complete = models.DateField()
    objects = MagentoSiteAuditManager()

    class Meta:
        ordering = ("date_complete", "magento_site__api_url")
        unique_together = ("magento_site", "date_complete")
        verbose_name = "Magento URL Audit"

    def __str__(self):
        return "{} for {}".format(
            self.date_complete.strftime("%d/%m/%Y"), self.magento_site.api_url
        )


class MagentoSiteCountryManager(models.Manager):
    def _create_country(self, magento_site, magento_country_id, country):
        x = self.model(
            magento_site=magento_site,
            magento_country_id=magento_country_id,
            country=country,
        )
        x.save()
        return x

    def _get_country(self, magento_site, magento_country_id):
        return self.model.objects.get(
            magento_site=magento_site, magento_country_id=magento_country_id
        )

    def current(self, magento_site):
        return self.model.objects.filter(magento_site=magento_site)

    def find_country(self, magento_site, magento_country_id):
        """Find the country ('finance.models.Country')."""
        try:
            magento_site_country = self._get_country(
                magento_site, magento_country_id
            )
        except self.model.DoesNotExist:
            raise MagentoError(
                "Invalid Magento Country ID: '{}' (site '{}') (did you run "
                "the 'magento_sync_settings' management command)?".format(
                    magento_country_id, magento_site.pk
                )
            )
        return magento_site_country.country

    def init_country(self, magento_site, magento_country_id, country):
        try:
            x = self._get_country(magento_site, magento_country_id)
            x.country = country
            x.save()
        except self.model.DoesNotExist:
            x = self._create_country(magento_site, magento_country_id, country)
        return x


class MagentoSiteCountry(models.Model):
    """Magento countries for a site.

    https://devdocs.magento.com/guides/m1x/api/soap/directory/directory_country.list.html

    31/07/2019 Looks like every record will be linked to a ``Country`` object.
    Not sure if this will work or not?

    """

    magento_site = models.ForeignKey(MagentoSite, on_delete=models.CASCADE)
    magento_country_id = models.CharField(max_length=10)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    objects = MagentoSiteCountryManager()

    class Meta:
        ordering = ("magento_site__pk", "magento_country_id")
        unique_together = ("magento_site", "magento_country_id")
        verbose_name = "Magento Site Countries"

    def __str__(self):
        return "{} {} (site {})".format(
            self.magento_country_id, self.country.name, self.magento_site.pk
        )


class MagentoSiteCustomerGroupManager(models.Manager):
    def _create_customer_group(
        self, magento_site, magento_customer_group_id, code
    ):
        x = self.model(
            magento_site=magento_site,
            magento_customer_group_id=magento_customer_group_id,
            code=code,
        )
        x.save()
        return x

    def current(self, magento_site):
        return self.model.objects.filter(magento_site=magento_site)

    def get_by_customer_group_id(self, magento_site, magento_customer_group_id):
        try:
            result = self.model.objects.get(
                magento_site=magento_site,
                magento_customer_group_id=magento_customer_group_id,
            )
        except self.model.DoesNotExist:
            raise MagentoError(
                "Invalid Magento Group ID: '{}' (site '{}') (did you run "
                "the 'magento_sync_settings' management command)?".format(
                    magento_customer_group_id, magento_site.pk
                )
            )
        return result

    # def group_id_on_account(self, magento_site, magento_customer_group_id):
    #     customer_group = self._get_by_group_id(
    #         magento_site, magento_customer_group_id
    #     )

    def init_customer_group(
        self, magento_site, magento_customer_group_id, code
    ):
        try:
            x = self.model.objects.get(
                magento_site=magento_site,
                magento_customer_group_id=magento_customer_group_id,
                code=code,
            )
            x.save()
        except self.model.DoesNotExist:
            x = self._create_customer_group(
                magento_site, magento_customer_group_id, code
            )
        return x


class MagentoSiteCustomerGroup(models.Model):
    magento_site = models.ForeignKey(MagentoSite, on_delete=models.CASCADE)
    magento_customer_group_id = models.IntegerField()
    code = models.CharField(max_length=100)
    objects = MagentoSiteCustomerGroupManager()

    class Meta:
        ordering = (
            "magento_site__pk",
            "magento_customer_group_id",
        )
        unique_together = ("magento_site", "magento_customer_group_id")
        verbose_name = "Magento Customer Group"

    def __str__(self):
        return "{}".format(self.code)


class MagentoSitePaymentMethodManager(models.Manager):
    def _create_magento_site_payment_method(
        self, magento_site, slug, description
    ):
        with transaction.atomic():
            processor = PaymentProcessor.objects.create_payment_processor(
                description
            )
            x = self.model(
                magento_site=magento_site,
                slug=slug,
                payment_processor=processor,
            )
            x.save()
        return x

    def current(self, magento_site):
        return self.model.objects.filter(magento_site=magento_site)

    def init_magento_site_payment_method(self, magento_site, slug, description):
        try:
            x = self.model.objects.get(magento_site=magento_site, slug=slug)
        except self.model.DoesNotExist:
            x = self._create_magento_site_payment_method(
                magento_site, slug, description
            )
        return x


class MagentoSitePaymentMethod(TimeStampedModel):
    """Link the payment method for a Magento site to a payment processor."""

    magento_site = models.ForeignKey(MagentoSite, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=30)
    on_account = models.BooleanField(default=False)
    payment_processor = models.ForeignKey(
        PaymentProcessor, on_delete=models.CASCADE, related_name="+"
    )
    objects = MagentoSitePaymentMethodManager()

    class Meta:
        ordering = ("slug",)
        unique_together = ("magento_site", "slug")
        verbose_name = "Magento Payment Method"

    def __str__(self):
        return "{} for {} ({})".format(
            self.payment_processor.description,
            self.magento_site.description,
            self.slug,
        )
