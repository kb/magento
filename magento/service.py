# -*- encoding: utf-8 -*-
import attr
import json
import logging
import urllib.parse
import xmlrpc.client

from datetime import datetime
from decimal import Decimal, InvalidOperation
from django.core.serializers.json import DjangoJSONEncoder
from enum import Enum


logger = logging.getLogger(__name__)


def get_vat_rate(item):
    base_row_total = to_decimal(item.get("base_row_total"))
    base_tax_amount = to_decimal(item.get("base_tax_amount"))
    if base_tax_amount == Decimal() or base_row_total == Decimal():
        vat_rate = Decimal()
    else:
        vat_rate = base_tax_amount / base_row_total
    return vat_rate


def to_decimal(item):
    """Convert a string to a decimal value.

    Essentially a check to make sure no ``None`` values are passed to the
    database that doesn't accept nones.

    """
    try:
        return Decimal(item or "0")
    except InvalidOperation:
        raise MagentoClientError(
            "Cannot convert '{}' to a decimal value".format(item)
        )


def to_integer(item):
    """Convert a string to an integer value."""
    if item is None:
        result = None
    else:
        try:
            result = int(item)
        except ValueError:
            raise MagentoClientError(
                "Cannot convert '{}' to an integer value".format(item)
            )
    return result


@attr.s
class Line:
    # MagentoProductData
    product_data = attr.ib()
    price = attr.ib()
    quantity = attr.ib()
    discount = attr.ib()
    net = attr.ib()
    vat = attr.ib()

    def as_dict(self):
        return {
            "product": self.product_data.slug,
            "quantity": self.quantity,
            "price": self.price,
            "net": self.net,
            "discount": self.discount,
            "vat": self.vat,
            "gross": self.gross,
        }

    @property
    def gross(self):
        return self.net + self.vat - self.discount

    def is_valid(self):
        result = False
        if self.net == (self.price * abs(self.quantity)):
            result = True
        return result

    @property
    def vat_rate(self):
        return get_vat_rate(
            {"base_row_total": str(self.net), "base_tax_amount": str(self.vat)}
        )


class MagentoClientError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


@attr.s
class MagentoAddressData:
    address_one = attr.ib()
    address_two = attr.ib()
    town = attr.ib()
    postal_code = attr.ib()
    admin_area = attr.ib()
    country_id = attr.ib()


@attr.s
class MagentoCustomerData:
    customer_id = attr.ib()
    group_id = attr.ib()


@attr.s
class MagentoInvoiceData:
    base_grand_total = attr.ib()
    create_issue_messages = attr.ib()
    currency_code = attr.ib()
    exchange_rate = attr.ib()
    invoice_date = attr.ib()
    # this may be the link from a credit note to an invoice
    invoice_id = attr.ib()
    invoice_increment_id = attr.ib()
    is_credit = attr.ib()
    # [Line]
    lines = attr.ib()
    # ``MagentoOrderData``
    order = attr.ib()
    # duplicated in ``MagentoOrderData``
    order_increment_id = attr.ib()


@attr.s
class MagentoOrderData:
    # ``MagentoAddressData``
    address = attr.ib()
    company = attr.ib()
    customer_address_id = attr.ib()
    customer_id = attr.ib()
    email = attr.ib()
    first_name = attr.ib()
    last_name = attr.ib()
    order_id = attr.ib()
    # copy of ``order_increment_id`` in ``MagentoInvoiceData`` do we need this?
    order_increment_id = attr.ib()
    paid = attr.ib()
    payment_method = attr.ib()
    phone = attr.ib()
    vat_number = attr.ib()


class MagentoProductType(Enum):
    # used for invoice / credit notes with missing lines (``missing_product``)
    MISSING = 1
    PRODUCT = 2
    # shipping costs (``shipping_product``)
    SHIPPING = 3


@attr.s
class MagentoProductData:
    # MagentoProductType
    product_type = attr.ib()
    slug = attr.ib()
    name = attr.ib()
    description = attr.ib()
    price = attr.ib()

    @property
    def is_missing(self):
        return self.product_type == MagentoProductType.MISSING

    @property
    def is_shipping(self):
        return self.product_type == MagentoProductType.SHIPPING


@attr.s
class Total:
    shipping_net = attr.ib()
    shipping_vat = attr.ib()
    discount = attr.ib()
    vat = attr.ib()
    gross = attr.ib()

    def as_dict(self):
        return {
            "shipping_net": self.shipping_net,
            "shipping_vat": self.shipping_vat,
            "discount": self.discount,
            "net": self.net,
            "vat": self.vat,
            "gross": self.gross,
        }

    def is_valid(self, lines):
        result = False
        discount = net = vat = Decimal()
        for line in lines:
            discount = discount + line.discount
            net = net + line.net
            vat = vat + line.vat
        if self.vat == vat:
            if self.gross == (net + vat - discount):
                result = True
        return result

    @property
    def net(self):
        return self.gross - self.vat

    @property
    def vat_rate(self):
        return get_vat_rate(
            {"base_row_total": str(self.net), "base_tax_amount": str(self.vat)}
        )


class MagentoClient:
    CREDIT_MULTIPLIER = Decimal("-1.00")
    INVOICE_MULTIPLIER = Decimal("1.00")

    def __init__(self, magento_site):
        self.client = None
        self.magento_site = magento_site
        self.session = None

    def _add_line_to_invoice(self, price, discount, vat, net, base_row_total):
        """Do we add this line to the invoice?

        It looks like *duplicate* lines have a ``base_row_total`` of ``None``.

        https://www.kbsoftware.co.uk/crm/ticket/4432/

        """
        result = False
        if base_row_total is None:
            result = bool(discount or vat or net)
        else:
            result = True
        return result

    def _api_call(self, path, params=None):
        client = self.connect()
        try:
            return client.call(self.session, path, params or [])
        except xmlrpc.client.Fault as e:
            logger.error(path, params)
            raise MagentoClientError(e)

    def _api_url(self):
        """Get the API URL.

        How to enable Magento XML-RPC?
        https://stackoverflow.com/questions/12115954/how-to-enable-magento-xml-rpc

        """
        return urllib.parse.urljoin(self.magento_site.api_url, "api/xmlrpc")

    def _create_adjustment_lines(self, total, shipping_line):
        lines = []
        if shipping_line:
            lines.append(shipping_line)
        zero = Decimal()
        lines.append(
            Line(
                # no lines, so use ``MISSING`` (``missing_product``)
                product_data=MagentoProductData(
                    product_type=MagentoProductType.MISSING,
                    slug=None,
                    name=None,
                    description=None,
                    price=None,
                ),
                price=total.net
                - (shipping_line.net if shipping_line else zero),
                quantity=(
                    shipping_line.quantity if shipping_line else Decimal("1")
                ),
                discount=Decimal(),
                net=total.net - (shipping_line.net if shipping_line else zero),
                vat=total.vat - (shipping_line.vat if shipping_line else zero),
            )
        )
        return lines

    def _created_at_filter(self, transaction_date):
        date_str = transaction_date.strftime("%Y-%m-%d")
        return {
            "created_at": {
                "from": "{} 00:00:00".format(date_str),
                "to": "{} 23:59:59".format(date_str),
            }
        }

    def _invoice_credit_data(self, data, is_credit):
        currency_code = data["base_currency_code"]
        exchange_rate = Decimal(data["base_to_global_rate"])
        invoice_date_str = data["created_at"][:10]
        invoice_date = datetime.strptime(invoice_date_str, "%Y-%m-%d").date()
        # The 'invoice_id' can be 'None'?
        invoice_id = data["invoice_id"]
        if invoice_id:
            invoice_id = int(invoice_id)
        invoice_increment_id = int(data["increment_id"])
        lines, create_issue_message = self._invoice_credit_lines(
            data, is_credit, invoice_increment_id
        )
        create_issue_messages = []
        if create_issue_message:
            create_issue_messages = [create_issue_message]
        if not exchange_rate:
            raise MagentoClientError(
                "{} {} does not have an exchange rate "
                "(currency {})".format(
                    "Credit note" if is_credit else "Invoice",
                    invoice_increment_id,
                    currency_code,
                )
            )
        return MagentoInvoiceData(
            base_grand_total=Decimal(data["base_grand_total"]),
            create_issue_messages=create_issue_messages,
            currency_code=currency_code,
            exchange_rate=exchange_rate,
            invoice_date=invoice_date,
            invoice_id=invoice_id,
            invoice_increment_id=invoice_increment_id,
            is_credit=is_credit,
            lines=lines,
            order=None,
            order_increment_id=int(data["order_increment_id"]),
        )

    def _invoice_credit_lines(self, data, is_credit, increment_id):
        create_issue_message = None
        if is_credit:
            quantity_multiplier = self.CREDIT_MULTIPLIER
        else:
            quantity_multiplier = self.INVOICE_MULTIPLIER
        total = self._invoice_total(data)
        lines = self._invoice_product_lines(data, quantity_multiplier)
        shipping_line = self._invoice_shipping_line(total, quantity_multiplier)
        if shipping_line:
            lines.append(shipping_line)
        if lines:
            pass
        elif is_credit:
            # no invoice lines (and no shipping)
            lines.append(
                Line(
                    # no lines, so use ``MISSING`` (``missing_product``)
                    product_data=MagentoProductData(
                        product_type=MagentoProductType.MISSING,
                        slug=None,
                        name=None,
                        description=None,
                        price=None,
                    ),
                    price=total.net,
                    quantity=Decimal("-1.00"),
                    discount=total.discount,
                    net=total.net,
                    vat=total.vat,
                )
            )
        else:
            caption = "Credit" if is_credit else "Invoice"
            message = "{} '{}' has no lines".format(caption, increment_id)
            logger.error(message)
            logger.error(json.dumps(data, indent=4))
            raise MagentoClientError(message)
        self._is_valid_lines(lines, is_credit, increment_id)
        try:
            self._is_valid_total(total, lines, is_credit, increment_id)
        except MagentoClientError:
            lines = self._create_adjustment_lines(total, shipping_line)
            # check the balances again
            self._is_valid_lines(lines, is_credit, increment_id)
            self._is_valid_total(total, lines, is_credit, increment_id)
            create_issue_message = "Line totals adjusted to balance"
        return lines, create_issue_message

    def _invoice_product_lines(self, data, quantity_multiplier):
        result = []
        items = data["items"]
        for item in items:
            price = abs(to_decimal(item["base_price"]))
            quantity = abs(to_decimal(item["qty"])) * quantity_multiplier
            discount = abs(to_decimal(item["base_discount_amount"]))
            vat = abs(to_decimal(item["base_tax_amount"]))
            base_row_total = item["base_row_total"]
            net = abs(to_decimal(base_row_total))
            if abs(quantity) == Decimal("1"):
                if price == net:
                    pass
                else:
                    rounded_price = price.quantize(Decimal(".01"))
                    if net == rounded_price:
                        price = net
                    else:
                        logger.error(
                            "Cannot fix price {} == net {} "
                            "(rounded price {})".format(
                                price, net, rounded_price
                            )
                        )
            if self._add_line_to_invoice(
                price, discount, vat, net, base_row_total
            ):
                product_data = MagentoProductData(
                    product_type=MagentoProductType.PRODUCT,
                    slug=item["sku"],
                    name=item["name"],
                    description=item.get("description") or "",
                    price=to_decimal(item["base_price"]),
                )
                result.append(
                    Line(
                        product_data=product_data,
                        price=price,
                        quantity=quantity,
                        discount=discount,
                        vat=vat,
                        net=net,
                    )
                )
        return result

    def _invoice_shipping_line(self, total, quantity_multiplier):
        result = None
        if total.shipping_net or total.shipping_vat:
            result = Line(
                product_data=MagentoProductData(
                    product_type=MagentoProductType.SHIPPING,
                    slug=None,
                    name=None,
                    description=None,
                    price=None,
                ),
                price=total.shipping_net,
                quantity=quantity_multiplier,
                discount=Decimal(),
                net=total.shipping_net,
                vat=total.shipping_vat,
            )
        return result

    def _invoice_total(self, data):
        return Total(
            shipping_net=abs(to_decimal(data.get("base_shipping_amount"))),
            shipping_vat=abs(to_decimal(data.get("base_shipping_tax_amount"))),
            discount=abs(to_decimal(data["base_discount_amount"])),
            vat=abs(to_decimal(data["base_tax_amount"])),
            gross=abs(to_decimal(data["base_grand_total"])),
        )

    def _is_valid_lines(self, lines, is_credit, increment_id):
        for line in lines:
            if line.is_valid():
                pass
            else:
                caption = "Credit" if is_credit else "Invoice"
                message = "{} '{}' has an invalid line: {}".format(
                    caption,
                    increment_id,
                    json.dumps(line.as_dict(), indent=4, cls=DjangoJSONEncoder),
                )
                logger.error(message)
                raise MagentoClientError(message)

    def _is_valid_total(self, total, lines, is_credit, increment_id):
        if total.is_valid(lines):
            pass
        else:
            caption = "Credit" if is_credit else "Invoice"
            message = "{} (or credit note) '{}' is invalid: {}".format(
                caption,
                increment_id,
                json.dumps(total.as_dict(), indent=4, cls=DjangoJSONEncoder),
            )
            logger.error(message)
            x = []
            for line in lines:
                x.append(line.as_dict())
            logger.error(json.dumps(x, indent=4, cls=DjangoJSONEncoder))
            raise MagentoClientError(message)

    def _split_street_address(self, street):
        if "\n" in street:
            a = street.split("\n", 1)
        elif "," in street:
            a = street.split(",", 1)
        else:
            a = [street, ""]
        return a

    def connect(self):
        """Connect to the XML-RPC client.

        .. note:: We only set ``self.client`` if we can get a session.

        """
        if self.client and self.session:
            return self.client
        else:
            url = self._api_url()
            client = xmlrpc.client.ServerProxy(url)
            self.session = client.login(
                self.magento_site.username, self.magento_site.password
            )
            self.client = client
            return self.client

    def country_list(self):
        """Retrieve the list of countries from Magento."""
        return self._api_call("country.list")

    def customer(self, customer_id):
        """Returns the information for a customer, given it's customer ID."""
        return self._api_call("customer.info", [{"customer_id": customer_id}])

    def customer_data(self, customer_id):
        """Get the Magento customer data.

        Other fields are::

          company_name, confirmation, created_at, created_in, default_billing,
          default_shipping, disable_auto_group_change, dob, email, firstname,
          gender, increment_id, lastname, mailchimp_store_view, middlename,
          password_created_at, password_hash, prefix, rp_customer_id, rp_token,
          rp_token_created_at, store_id, suffix, tax_class_id, taxvat,
          updated_at, website_id

        """
        if customer_id is None:
            result = None
        else:
            data = self.customer(customer_id)
            result = MagentoCustomerData(
                customer_id=data["customer_id"],
                group_id=data["group_id"],
            )
        return result

    def customer_group_list(self):
        return self._api_call("customer_group.list")

    def filter_credit_notes(self, transaction_date):
        credit_note_filter = self._created_at_filter(transaction_date)
        return self.credit_memo_list_filters(credit_note_filter)

    def filter_invoice(self, transaction_date):
        invoice_filter = self._created_at_filter(transaction_date)
        return self.invoice_list(invoice_filter)

    def get_all_credit_memos(self):
        """Some filter to give all of the memos."""
        credit_memo_filter = {"grand_total": {"gteq": "0"}}
        credit_memo_list = self.credit_memo_list_filters(credit_memo_filter)
        return credit_memo_list

    def invoice_id(self, invoice_id):
        """Returns the information in an invoice, given it's invoice ID."""
        return self._api_call(
            "sales_order_invoice.info", [{"increment_id": invoice_id}]
        )

    def invoice_id_data(self, increment_id):
        data = self.invoice_id(increment_id)
        magento_invoice_data = self._invoice_credit_data(data, is_credit=False)
        if increment_id == magento_invoice_data.invoice_increment_id:
            pass
        else:
            raise MagentoClientError(
                "Magento invoice increment_id '{}' does not match "
                "the returned ID '{}'".format(
                    increment_id, magento_invoice_data.invoice_increment_id
                )
            )
        return magento_invoice_data

    def invoice_list(self, filters):
        """Retrieves a list of all invoices that meet the filter.

        .. note:: Does not return all details in the invoice.

        """
        return self._api_call("sales_order_invoice.list", [filters])

    def invoice(self, filters):
        """Returns the first invoice that meets the filter."""
        return self._api_call("sales_order_invoice.info", [filters])

    def sales_order(self, increment_id):
        """Download data for a sales order based on increment id.

        Orders are identified by the ``increment_id``.
        We know this because the web UI says ``Order # 100015171``.

        """
        return self._api_call(
            "sales_order.info", [{"increment_id": increment_id}]
        )

    def sales_order_data(self, increment_id):
        """Fetches the order associated with the ``order_increment_id``.

        Uses this list to get the full order with it's details for the first
        element in that list.

        Copied code from:

        - ``order_details_extract``
        - ``extract_order_address``
        - ``extract_order_invoice_info``
        - ``retrieve_order``

        ... into this function...

        """
        data = self.sales_order(increment_id)
        customer_id = to_integer(data["customer_id"])
        if data["total_paid"] == data["total_invoiced"]:
            paid = True
            payment_method = data["payment"]["method"]
        else:
            paid = False
            payment_method = None
        billing_address = data["billing_address"]
        # email
        email = data["customer_email"]
        if not email:
            email = billing_address["email"]
        if not email:
            raise MagentoClientError(
                "email not found for order {}".format(increment_id)
            )
        # name
        firstname = billing_address["firstname"]
        if len(firstname) > 29:
            firstname = firstname.split(" ", 1)[0]
        lastname = billing_address["lastname"]
        company = billing_address["company"]
        # address
        customer_address_id = billing_address["customer_address_id"]
        street_address = self._split_street_address(billing_address["street"])
        address = MagentoAddressData(
            address_one=street_address[0].strip(),
            address_two=street_address[1].strip(),
            town=billing_address["city"],
            postal_code=billing_address["postcode"] or "",
            country_id=billing_address["country_id"],
            admin_area=billing_address["region"] or "",
        )
        # phone
        phone = billing_address["telephone"]
        # vat_number
        vat_number = data["customer_taxvat"]
        if not vat_number:
            vat_number = billing_address["vat_id"]
        magento_order_data = MagentoOrderData(
            address=address,
            company=company or "",
            customer_address_id=customer_address_id,
            customer_id=customer_id,
            email=email,
            first_name=firstname,
            last_name=lastname,
            order_id=data["order_id"],
            order_increment_id=int(data["increment_id"]),
            paid=paid,
            payment_method=payment_method,
            phone=phone,
            vat_number=vat_number,
        )
        if increment_id == magento_order_data.order_increment_id:
            pass
        else:
            raise MagentoClientError(
                "Magento sales order increment_id '{}' does not match "
                "the returned ID '{}'".format(
                    increment_id, magento_order_data.order_increment_id
                )
            )
        return magento_order_data

    # def sales_order_list(self, filters):
    #    """
    #    Download data for all sales orders with filter applied.
    #    """
    #    return self._api_call("sales_order.list", [filters])

    def address(self, addressid):
        """
        retrieves the address for a given address id
        """
        return self._api_call(
            "customer_address.info", [{"addressId": addressid}]
        )

    def customer(self, customerid):
        """
        retrieves the customer information for a given customer id
        """
        return self._api_call("customer.info", [{"customer_id": customerid}])

    def credit_memo(self, creditmemo_id):
        """Retrieves the credit memo associated with an invoice id."""
        return self._api_call(
            "order_creditmemo.info", [{"creditmemo_id": creditmemo_id}]
        )

    def credit_memo_data(self, credit_note_increment_id):
        data = self.credit_memo(credit_note_increment_id)
        magento_credit_data = self._invoice_credit_data(data, is_credit=True)
        return magento_credit_data

    def credit_memo_list_id(self, invoice_id):
        """
        retrieves the credit memo associated with an invoice id
        """
        return self._api_call(
            "order_creditmemo.list", [{"invoice_id": invoice_id}]
        )

    def credit_memo_list_filters(self, filters):
        """
        retrieves the credit memo associated with an invoice id
        """
        return self._api_call("order_creditmemo.list", [filters])
