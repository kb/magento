# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from .models import SyncSettings


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def magento_sync_settings():
    count = 0
    logger.info("Sync Magento Settings (country codes etc)...")
    sync_settings = SyncSettings()
    count = sync_settings.sync()
    logger.info("Sync Magento Settings ({} records) - Complete".format(count))
    return count
