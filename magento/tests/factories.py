# -*- encoding: utf-8 -*-
import factory

from datetime import date

from finance.tests.factories import CountryFactory
from invoice.tests.factories import InvoiceFactory, PaymentProcessorFactory
from magento.models import (
    MagentoInvoice,
    MagentoSettings,
    MagentoSite,
    MagentoSiteAudit,
    MagentoSiteCountry,
    MagentoSiteCustomerGroup,
    MagentoSitePaymentMethod,
)
from stock.tests.factories import ProductFactory


class MagentoInvoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MagentoInvoice

    invoice = factory.SubFactory(InvoiceFactory)

    @factory.sequence
    def magento_invoice_id(n):
        return n + 1

    @factory.sequence
    def magento_invoice_increment_id(n):
        return n + 1


class MagentoSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MagentoSettings

    missing_product = factory.SubFactory(ProductFactory)
    shipping_product = factory.SubFactory(ProductFactory)


class MagentoSiteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MagentoSite

    @factory.sequence
    def description(n):
        return "description_{}".format(n)


class MagentoSiteAuditFactory(factory.django.DjangoModelFactory):
    date_complete = date(2019, 4, 9)

    class Meta:
        model = MagentoSiteAudit

    magento_site = factory.SubFactory(MagentoSiteFactory)


class MagentoSiteCountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MagentoSiteCountry

    magento_site = factory.SubFactory(MagentoSiteFactory)
    country = factory.SubFactory(CountryFactory)

    @factory.sequence
    def magento_country_id(n):
        return "{}".format(n + 1)

    # def name(n):
    #    return "name_{}".format(n + 1)

    # def iso2_code(n):
    #    return "{}".format(n + 1)

    # def iso3_code(n):
    #    return "{}".format(n + 1)


class MagentoSiteCustomerGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MagentoSiteCustomerGroup

    magento_site = factory.SubFactory(MagentoSiteFactory)

    @factory.sequence
    def code(n):
        return "code-{}".format(n + 1)

    @factory.sequence
    def magento_customer_group_id(n):
        return n + 1


class MagentoSitePaymentMethodFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MagentoSitePaymentMethod

    magento_site = factory.SubFactory(MagentoSiteFactory)
    payment_processor = factory.SubFactory(PaymentProcessorFactory)

    @factory.sequence
    def slug(n):
        return "slug_{}".format(n)
