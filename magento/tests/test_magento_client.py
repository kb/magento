# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from unittest import mock

from magento.service import (
    get_vat_rate,
    Line,
    MagentoAddressData,
    MagentoClient,
    MagentoClientError,
    MagentoInvoiceData,
    MagentoOrderData,
    MagentoProductData,
    MagentoProductType,
    to_decimal,
    to_integer,
    Total,
)
from stock.tests.factories import ProductFactory
from .factories import MagentoSettingsFactory, MagentoSiteFactory


@pytest.mark.parametrize(
    "price,discount,vat,net,base_row_total,expect",
    [
        (None, None, None, None, None, False),
        (Decimal("0"), Decimal("0"), Decimal("0"), Decimal("0"), None, False),
        (Decimal("0"), Decimal("0"), Decimal("0"), Decimal("0"), "0", True),
        (Decimal("0"), Decimal("0"), Decimal("0"), Decimal("0"), "1", True),
        (Decimal("1"), Decimal("0"), Decimal("0"), Decimal("0"), None, False),
        (Decimal("0"), Decimal("1"), Decimal("0"), Decimal("0"), None, True),
        (Decimal("0"), Decimal("0"), Decimal("1"), Decimal("0"), None, True),
        (Decimal("0"), Decimal("0"), Decimal("0"), Decimal("1"), None, True),
    ],
)
@pytest.mark.django_db
def test_add_line_to_invoice(price, discount, vat, net, base_row_total, expect):
    magento = MagentoClient(MagentoSiteFactory())
    assert (
        magento._add_line_to_invoice(price, discount, vat, net, base_row_total)
        is expect
    )


@pytest.mark.django_db
def test_create_adjustment_lines():
    missing_product = ProductFactory(name="missing")
    shipping_product = ProductFactory(name="shipping")
    MagentoSettingsFactory(
        missing_product=missing_product, shipping_product=shipping_product
    )
    total = Total(
        shipping_net=Decimal("50"),
        shipping_vat=Decimal("10"),
        discount=Decimal(),
        vat=Decimal("30"),
        gross=Decimal("180"),
    )
    magento = MagentoClient(MagentoSiteFactory())
    shipping_line = magento._invoice_shipping_line(
        total, MagentoClient.INVOICE_MULTIPLIER
    )
    expect = [
        shipping_line,
        Line(
            product_data=MagentoProductData(
                product_type=MagentoProductType.MISSING,
                slug=None,
                name=None,
                description=None,
                price=None,
            ),
            price=Decimal("100"),
            quantity=Decimal("1"),
            discount=Decimal(),
            net=Decimal("100"),
            vat=Decimal("20"),
        ),
    ]
    result = magento._create_adjustment_lines(total, shipping_line)
    assert expect == result


@pytest.mark.django_db
def test_get_vat_rate_with_none():
    item = {"base_tax_amount": None}
    assert get_vat_rate(item) == 0


@pytest.mark.django_db
def test_get_vat_rate_with_none_v2():
    item = {"base_tax_amount": 10, "base_row_total": 0}
    assert get_vat_rate(item) == 0


@pytest.mark.django_db
def test_get_vat_rate_with_value():
    item = {"base_tax_amount": 10, "base_row_total": 40}
    assert get_vat_rate(item) == 0.25


@pytest.mark.parametrize(
    "is_credit,expect",
    [
        (False, "Invoice 15555 does not have an exchange rate (currency GBP)"),
        (
            True,
            "Credit note 15555 does not have an exchange rate (currency GBP)",
        ),
    ],
)
@pytest.mark.django_db
def test_invoice_credit_data_zero_exchange_rate(is_credit, expect):
    MagentoSettingsFactory()
    magento = MagentoClient(MagentoSiteFactory())
    with pytest.raises(MagentoClientError) as e:
        magento._invoice_credit_data(
            {
                "base_currency_code": "GBP",
                "base_discount_amount": "0.0000",
                "base_grand_total": "120.0000",
                "base_tax_amount": "20.0000",
                "base_to_global_rate": "0.0000",
                "created_at": "2018-01-18 13:38:45",
                "increment_id": "15555",
                "invoice_id": "18214",
                "items": [
                    {
                        "base_discount_amount": 0,
                        "base_price": 100,
                        "base_row_total": 100,
                        "base_tax_amount": 20,
                        "description": "Colours",
                        "name": "Colour",
                        "qty": "1",
                        "sku": "green",
                    }
                ],
                "order_increment_id": "100007777",
            },
            is_credit=is_credit,
        )
    assert expect in str(e.value)


@pytest.mark.django_db
def test_create_adjustment_lines_no_shipping():
    missing_product = ProductFactory(name="missing")
    shipping_product = ProductFactory(name="shipping")
    MagentoSettingsFactory(
        missing_product=missing_product, shipping_product=shipping_product
    )
    total = Total(
        shipping_net=Decimal(),
        shipping_vat=Decimal(),
        discount=Decimal(),
        vat=Decimal("20"),
        gross=Decimal("120"),
    )
    magento = MagentoClient(MagentoSiteFactory())
    shipping_line = magento._invoice_shipping_line(
        total, MagentoClient.INVOICE_MULTIPLIER
    )
    expect = [
        Line(
            product_data=MagentoProductData(
                product_type=MagentoProductType.MISSING,
                slug=None,
                name=None,
                description=None,
                price=None,
            ),
            price=Decimal("100"),
            quantity=Decimal("1"),
            discount=Decimal(),
            net=Decimal("100"),
            vat=Decimal("20"),
        )
    ]
    result = magento._create_adjustment_lines(total, shipping_line)
    # from prettyprinter import install_extras, pprint
    # install_extras()
    # pprint(expect)
    # pprint(result)
    assert expect == result


@pytest.mark.django_db
def test_invoice_credit_data_no_lines_or_shipping():
    """

    Replace ``test_create_or_find_credit_note_no_lines_or_shipping``
    in ``magento/tests/test_models.py``.

    """
    magento = MagentoClient(MagentoSiteFactory())
    assert MagentoInvoiceData(
        base_grand_total=Decimal("120.00"),
        currency_code="GBP",
        create_issue_messages=[],
        exchange_rate=Decimal("1.000"),
        invoice_date=date(2018, 1, 18),
        invoice_increment_id=100005555,
        invoice_id=18214,
        is_credit=True,
        lines=[
            Line(
                price=Decimal("100.00"),
                quantity=Decimal("-1.00"),
                discount=Decimal(),
                net=Decimal("100.00"),
                vat=Decimal("20.00"),
                product_data=MagentoProductData(
                    product_type=MagentoProductType.MISSING,
                    slug=None,
                    name=None,
                    description=None,
                    price=None,
                ),
            )
        ],
        order_increment_id=100007777,
        order=None,
    ) == magento._invoice_credit_data(
        {
            "base_currency_code": "GBP",
            "base_discount_amount": "0.0000",
            "base_grand_total": "120.0000",
            "base_tax_amount": "20.0000",
            "base_to_global_rate": "1.0000",
            "created_at": "2018-01-18 13:38:45",
            "increment_id": "100005555",
            "invoice_id": "18214",
            "items": [],
            "order_increment_id": "100007777",
        },
        is_credit=True,
    )


@pytest.mark.django_db
def test_invoice_product_lines():
    magento = MagentoClient(MagentoSiteFactory())
    result = magento._invoice_product_lines(
        {
            "items": [
                {
                    "sku": "apple",
                    "name": "Apple",
                    "description": "Crumble",
                    "base_price": "50.00",
                    "qty": "2.00",
                    "base_discount_amount": "0.00",
                    "base_tax_amount": "20.00",
                    "base_row_total": "100.00",
                    "base_row_total_incl_tax": "120.00",
                }
            ]
        },
        MagentoClient.INVOICE_MULTIPLIER,
    )
    assert [
        Line(
            product_data=MagentoProductData(
                product_type=MagentoProductType.PRODUCT,
                slug="apple",
                name="Apple",
                description="Crumble",
                price=Decimal("50.00"),
            ),
            price=Decimal("50.00"),
            quantity=Decimal("2.00"),
            discount=Decimal(),
            net=Decimal("100.00"),
            vat=Decimal("20.00"),
        )
    ] == result


@pytest.mark.django_db
def test_invoice_shipping_line():
    magento = MagentoClient(MagentoSiteFactory())
    result = magento._invoice_shipping_line(
        Total(
            shipping_net=Decimal("10.00"),
            shipping_vat=Decimal("2.00"),
            discount=Decimal(),
            vat=Decimal("20.00"),
            gross=Decimal("100.00"),
        ),
        MagentoClient.INVOICE_MULTIPLIER,
    )
    assert (
        Line(
            product_data=MagentoProductData(
                product_type=MagentoProductType.SHIPPING,
                slug=None,
                name=None,
                description=None,
                price=None,
            ),
            price=Decimal("10.00"),
            quantity=Decimal("1.00"),
            discount=Decimal(),
            net=Decimal("10.00"),
            vat=Decimal("2.00"),
        )
        == result
    )


@pytest.mark.django_db
def test_invoice_total():
    magento = MagentoClient(MagentoSiteFactory())
    result = magento._invoice_total(
        {
            "base_shipping_amount": "10.00",
            "base_shipping_tax_amount": "12.00",
            "base_discount_amount": "0",
            "base_tax_amount": "20.00",
            "base_grand_total": "120.00",
        }
    )
    assert (
        Total(
            shipping_net=Decimal("10.00"),
            shipping_vat=Decimal("12.00"),
            discount=Decimal(),
            vat=Decimal("20.00"),
            gross=Decimal("120.00"),
        )
        == result
    )
    # assert "S" == result.vat_code.slug
    assert Decimal("0.20") == result.vat_rate


@pytest.mark.django_db
def test_sales_order_data_increment_id_does_not_match():
    magento = MagentoClient(MagentoSiteFactory())
    with mock.patch("magento.service.MagentoClient.sales_order") as m2:
        m2.return_value = {
            "billing_address": {
                "city": "Exeter",
                "company": "d",
                "country_id": "UK",
                "customer_address_id": 22,
                "email": "patrick@kbsoftware.co.uk",
                "firstname": "P",
                "lastname": "Kimber",
                "postcode": "EX",
                "region": "Devon",
                "street": "Blue House, Pink Street",
                "telephone": "01837",
                "vat_id": "",
            },
            "customer_email": "",
            "customer_id": None,
            "customer_taxvat": "",
            "increment_id": "787878",
            "order_id": 33333,
            "original_increment_id": None,
            "payment": {"method": "Stripe"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        with pytest.raises(MagentoClientError) as e:
            magento.sales_order_data(565656)
    assert (
        "Magento sales order increment_id '565656' "
        "does not match the returned ID '787878'"
    ) in str(e.value)


@pytest.mark.django_db
def test_sales_order_data_with_an_order():
    magento = MagentoClient(MagentoSiteFactory())
    with mock.patch("magento.service.MagentoClient.sales_order") as m2:
        m2.return_value = {
            "billing_address": {
                "city": "Exeter",
                "company": "d",
                "country_id": "UK",
                "customer_address_id": 22,
                "firstname": "P",
                "lastname": "Kimber",
                "postcode": "EX",
                "region": "Devon",
                "street": "Blue House, Pink Street",
                "telephone": "01837",
            },
            "customer_email": "info@kbsoftware.co.uk",
            "customer_id": "818181",
            "customer_taxvat": "ABC",
            "increment_id": "565656",
            "order_id": 33333,
            "original_increment_id": None,
            "payment": {"method": "Stripe"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        result = magento.sales_order_data(565656)
    assert (
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="Blue House",
                address_two="Pink Street",
                town="Exeter",
                postal_code="EX",
                country_id="UK",
                admin_area="Devon",
            ),
            company="d",
            customer_address_id=22,
            customer_id=818181,
            email="info@kbsoftware.co.uk",
            first_name="P",
            last_name="Kimber",
            order_id=33333,
            order_increment_id=565656,
            paid=True,
            payment_method="Stripe",
            phone="01837",
            vat_number="ABC",
        )
        == result
    )


@pytest.mark.django_db
def test_sales_order_data_no_email():
    magento = MagentoClient(MagentoSiteFactory())
    with mock.patch("magento.service.MagentoClient.sales_order") as m2:
        m2.return_value = {
            "billing_address": {
                "city": "Exeter",
                "company": "d",
                "country_id": "UK",
                "customer_address_id": 22,
                "email": "",
                "firstname": "P",
                "lastname": "Kimber",
                "postcode": "EX",
                "region": "Devon",
                "street": "Blue House, Pink Street",
                "telephone": "01837",
            },
            "customer_email": "",
            "customer_id": "818181",
            "order_id": 33333,
            "original_increment_id": None,
            "payment": {"method": "Stripe"},
            "total_invoiced": 23,
            "total_paid": 23,
            "vat_number": "ABC",
        }
        with pytest.raises(MagentoClientError) as e:
            magento.sales_order_data(565656)
    assert "email not found for order 565656" in str(e.value)


@pytest.mark.parametrize(
    "customer_taxvat,vat_id,expect",
    [
        ("", "", ""),
        ("", "DEF", "DEF"),
        ("ABC", "", "ABC"),
        ("DEF", "DEF", "DEF"),
    ],
)
@pytest.mark.django_db
def test_sales_order_data_vat_number(customer_taxvat, vat_id, expect):
    magento = MagentoClient(MagentoSiteFactory())
    with mock.patch("magento.service.MagentoClient.sales_order") as m2:
        m2.return_value = {
            "billing_address": {
                "city": "Exeter",
                "company": "d",
                "country_id": "UK",
                "customer_address_id": 22,
                "email": "",
                "firstname": "P",
                "lastname": "Kimber",
                "postcode": "EX",
                "region": "Devon",
                "street": "Blue House, Pink Street",
                "telephone": "01837",
                "vat_id": vat_id,
            },
            "customer_email": "patrick@kbsoftware.co.uk",
            "customer_id": "818181",
            "customer_taxvat": customer_taxvat,
            "increment_id": "565656",
            "order_id": 33333,
            "original_increment_id": None,
            "payment": {"method": "Stripe"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        magento_order_data = magento.sales_order_data(565656)
    assert expect == magento_order_data.vat_number


@pytest.mark.django_db
def test_split_street_address_w_new_line():
    magento = MagentoClient(MagentoSiteFactory())
    street = "one,two\nthree"
    assert ["one,two", "three"] == magento._split_street_address(street)


@pytest.mark.django_db
def test_split_street_address_w_2_commas():
    magento = MagentoClient(MagentoSiteFactory())
    street = "one,two,three"
    assert ["one", "two,three"] == magento._split_street_address(street)


@pytest.mark.django_db
def test_split_street_address_w_2_new_lines():
    magento = MagentoClient(MagentoSiteFactory())
    street = "one\ntwo\nthree"
    assert ["one", "two\nthree"] == magento._split_street_address(street)


def test_to_decimal():
    assert Decimal("12.34") == to_decimal("12.34")


def test_to_decimal_none():
    item = None
    assert Decimal("0") == to_decimal(item)


def test_to_decimal_text():
    item = "test"
    with pytest.raises(MagentoClientError) as e:
        to_decimal(item)
    assert "Cannot convert 'test' to a decimal value" in str(e.value)


def test_to_integer():
    assert int(12) == to_integer("12")


def test_to_integer_none():
    assert to_integer(None) is None


def test_to_integer_text():
    with pytest.raises(MagentoClientError) as e:
        to_integer("test")
    assert "Cannot convert 'test' to an integer value" in str(e.value)
