# -*- encoding: utf-8 -*-
import pytest

from magento.models import INVOICE_PREFIX, MagentoInvoice
from invoice.tests.factories import InvoiceFactory
from .factories import MagentoInvoiceFactory


@pytest.mark.django_db
def test_create_magento_invoice():
    customer_id = 5454
    invoice_id = "5397"
    invoice_increment_id = "10000435"
    order_id = "5465"
    order_increment_id = "10004345"
    invoice = InvoiceFactory(number=invoice_id)
    magento_invoice = MagentoInvoice.objects.create_magento_invoice(
        invoice,
        invoice_id,
        invoice_increment_id,
        order_id,
        order_increment_id,
        customer_id,
    )
    assert magento_invoice.invoice == invoice
    assert magento_invoice.magento_invoice_increment_id == invoice_increment_id
    assert magento_invoice.magento_invoice_id == invoice_id
    assert magento_invoice.magento_order_id == order_id
    assert magento_invoice.magento_order_increment_id == order_increment_id
    assert magento_invoice.magento_customer_id == customer_id


@pytest.mark.django_db
def test_str():
    magento_invoice = MagentoInvoice.objects.create_magento_invoice(
        InvoiceFactory(number=23), 456, 1000456, None, None, 5454
    )
    assert "Invoice 000023: Magento Invoice ID: 456" == str(magento_invoice)


@pytest.mark.django_db
def test_str_2():
    invoice = InvoiceFactory(number=1111, prefix=INVOICE_PREFIX)
    x = MagentoInvoiceFactory(invoice=invoice, magento_invoice_id=123)
    assert "Invoice I001111: Magento Invoice ID: 123" == str(x)
