# -*- encoding: utf-8 -*-
import pytest

from stock.tests.factories import ProductFactory
from .factories import MagentoSettingsFactory


@pytest.mark.django_db
def test_str():
    assert "Missing product line 'apple', Shipping product 'orange'" == str(
        MagentoSettingsFactory(
            missing_product=ProductFactory(slug="apple"),
            shipping_product=ProductFactory(slug="orange"),
        )
    )
