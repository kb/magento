# -*- encoding: utf-8 -*-
import pytest

from datetime import date

from magento.models import MagentoError, MagentoSite
from .factories import (
    MagentoSiteFactory,
    MagentoSiteAuditFactory,
    MagentoSiteCustomerGroupFactory,
)


@pytest.mark.django_db
def test_audit():
    magento_site = MagentoSiteFactory()
    MagentoSiteAuditFactory(
        magento_site=magento_site, date_complete=date(2019, 1, 31)
    )
    MagentoSiteAuditFactory(magento_site=MagentoSiteFactory())
    MagentoSiteAuditFactory(
        magento_site=magento_site, date_complete=date(2019, 1, 30)
    )
    assert [date(2019, 1, 31), date(2019, 1, 30)] == [
        x.date_complete for x in magento_site.audit().order_by("-date_complete")
    ]


@pytest.mark.django_db
def test_customer_group_on_account():
    magento_site = MagentoSiteFactory()

    group_1 = MagentoSiteCustomerGroupFactory(magento_site=magento_site)
    group_2 = MagentoSiteCustomerGroupFactory(magento_site=magento_site)
    group_3 = MagentoSiteCustomerGroupFactory(magento_site=magento_site)
    group_4 = MagentoSiteCustomerGroupFactory(magento_site=magento_site)

    magento_site.on_account_customer_groups.add(group_1)
    magento_site.on_account_customer_groups.add(group_3)

    assert magento_site.customer_group_on_account(group_1) is True
    assert magento_site.customer_group_on_account(group_2) is False
    assert magento_site.customer_group_on_account(group_3) is True
    assert magento_site.customer_group_on_account(group_4) is False


@pytest.mark.django_db
def test_customer_group_on_account_invalid_site():
    magento_site_1 = MagentoSiteFactory()
    magento_site_2 = MagentoSiteFactory()
    group_1 = MagentoSiteCustomerGroupFactory(magento_site=magento_site_1)
    group_2 = MagentoSiteCustomerGroupFactory(magento_site=magento_site_2)
    magento_site_1.on_account_customer_groups.add(group_1)
    magento_site_2.on_account_customer_groups.add(group_2)
    with pytest.raises(MagentoError) as e:
        magento_site_1.customer_group_on_account(group_2)
    expect = (
        "Customer group '{}' is not linked to site '{}' "
        "(it is linked to site '{}')".format(
            group_2.pk,
            magento_site_1.pk,
            group_2.magento_site.pk,
        )
    )
    assert expect in str(e.value)


@pytest.mark.django_db
def test_is_download_complete():
    """Is the download complete for this site / date?"""
    magento_site = MagentoSiteFactory()
    date_complete = date(2019, 1, 31)
    MagentoSiteAuditFactory(
        magento_site=magento_site, date_complete=date_complete
    )
    assert magento_site.is_download_complete(date_complete) is True


@pytest.mark.django_db
def test_is_download_complete_not():
    """Is the download complete for this site / date?"""
    magento_site = MagentoSiteFactory()
    date_complete = date(2019, 1, 31)
    MagentoSiteAuditFactory(
        magento_site=magento_site, date_complete=date_complete
    )
    assert magento_site.is_download_complete(date(2019, 2, 1)) is False


@pytest.mark.django_db
def test_last_audit():
    magento_site = MagentoSiteFactory()
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date(2019, 1, 31)
    )
    MagentoSiteAuditFactory(
        magento_site=magento_site, date_complete=date(2019, 1, 30)
    )
    MagentoSiteAuditFactory(
        magento_site=magento_site, date_complete=date(2019, 1, 29)
    )
    last_audit = magento_site.last_audit()
    assert date(2019, 1, 30) == last_audit.date_complete


@pytest.mark.django_db
def test_last_audit_none():
    magento_site = MagentoSiteFactory()
    assert magento_site.last_audit() is None


@pytest.mark.django_db
def test_objects_is_download_complete():
    date_complete = date(2019, 1, 31)
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date_complete
    )
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date_complete
    )
    assert MagentoSite.objects.is_download_complete(date_complete) is True


@pytest.mark.django_db
def test_objects_is_download_complete_no_sites():
    assert MagentoSite.objects.is_download_complete(date(2019, 1, 31)) is False


@pytest.mark.django_db
def test_objects_is_download_complete_one_missing():
    date_complete = date(2019, 1, 31)
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date_complete
    )
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date(2019, 1, 30)
    )
    assert MagentoSite.objects.is_download_complete(date_complete) is False


@pytest.mark.django_db
def test_objects_is_valid_download():
    """Is it OK to download transactions on this date?"""
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date(2019, 1, 20)
    )
    assert MagentoSite.objects.is_valid_download(date(2019, 1, 21)) is True


@pytest.mark.django_db
def test_objects_is_valid_download_not():
    """Is it OK to download transactions on this date?"""
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date(2019, 1, 19)
    )
    assert MagentoSite.objects.is_valid_download(date(2019, 1, 21)) is False


@pytest.mark.django_db
def test_objects_is_valid_download_first_ever():
    """Is it OK to download transactions on this date?"""
    assert MagentoSite.objects.is_valid_download(date(2019, 1, 21)) is True


@pytest.mark.django_db
def test_objects_is_valid_download_try_again():
    """Is it OK to download transactions on this date?

    In this test, we downloaded Magento transactions for the day, but didn't
    complete supplementary tasks e.g. post to Xero.

    """
    MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(), date_complete=date(2019, 1, 20)
    )
    assert MagentoSite.objects.is_valid_download(date(2019, 1, 20)) is True


@pytest.mark.django_db
def test_ordering():
    MagentoSiteFactory(api_url="http://b", description="b")
    MagentoSiteFactory(api_url="http://a", description="a")
    assert ["a", "b"] == [x.description for x in MagentoSite.objects.all()]


@pytest.mark.django_db
def test_set_download_complete():
    magento_site = MagentoSiteFactory()
    date_complete = date(2019, 1, 31)
    assert magento_site.is_download_complete(date_complete) is False
    magento_site.set_download_complete(date_complete)
    assert magento_site.is_download_complete(date_complete) is True


@pytest.mark.django_db
def test_set_download_complete_duplicate():
    magento_site = MagentoSiteFactory(description="Apple")
    date_complete = date(2019, 1, 31)
    assert magento_site.is_download_complete(date_complete) is False
    magento_site.set_download_complete(date_complete)
    assert magento_site.is_download_complete(date_complete) is True
    with pytest.raises(MagentoError) as e:
        magento_site.set_download_complete(date_complete)
    assert (
        "The download for 31/01/2019 site 'Apple' has already been completed"
        in str(e.value)
    )


@pytest.mark.django_db
def test_str():
    magento_site = MagentoSiteFactory(api_url="http://localhost:8000")
    assert "http://localhost:8000" == str(magento_site)
