# -*- encoding: utf-8 -*-
import pytest

from datetime import date

from magento.models import MagentoError, MagentoSiteAudit
from .factories import MagentoSiteAuditFactory, MagentoSiteFactory


@pytest.mark.django_db
def test_ordering():
    MagentoSiteAuditFactory(magento_site=MagentoSiteFactory(api_url="b"))
    MagentoSiteAuditFactory(magento_site=MagentoSiteFactory(api_url="a"))
    assert ["a", "b"] == [
        x.magento_site.api_url for x in MagentoSiteAudit.objects.all()
    ]


@pytest.mark.django_db
def test_set_complete():
    magento_site = MagentoSiteFactory()
    assert 0 == MagentoSiteAudit.objects.count()
    MagentoSiteAudit.objects.set_complete(magento_site, date(2019, 1, 31))
    assert 1 == MagentoSiteAudit.objects.count()
    magento_site_audit = MagentoSiteAudit.objects.first()
    assert date(2019, 1, 31) == magento_site_audit.date_complete
    assert magento_site == magento_site_audit.magento_site


@pytest.mark.django_db
def test_set_complete_duplicate():
    magento_site = MagentoSiteFactory()
    MagentoSiteAudit.objects.set_complete(magento_site, date(2019, 1, 31))
    with pytest.raises(MagentoError) as e:
        MagentoSiteAudit.objects.set_complete(magento_site, date(2019, 1, 31))
    assert (
        "The Magento import for 31/01/2019 has already been completed"
        in str(e.value)
    )


@pytest.mark.django_db
def test_str():
    audit = MagentoSiteAuditFactory(
        magento_site=MagentoSiteFactory(api_url="http://localhost:8000")
    )
    assert "09/04/2019 for http://localhost:8000" == str(audit)
