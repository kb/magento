# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from finance.tests.factories import CountryFactory
from magento.models import MagentoError, MagentoSiteCountry
from .factories import MagentoSiteFactory, MagentoSiteCountryFactory


@pytest.mark.django_db
def test_find_country():
    country = CountryFactory()
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site, magento_country_id="CC", country=country
    )
    # Another Magento site linked to the same 'Country'...
    MagentoSiteCountryFactory(
        magento_site=MagentoSiteFactory(),
        magento_country_id="CC",
        country=country,
    )
    assert country == MagentoSiteCountry.objects.find_country(
        magento_site, "CC"
    )


@pytest.mark.django_db
def test_find_country_does_not_exist():
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="AA",
        country=CountryFactory(iso2_code="AA"),
    )
    # Another Magento site linked to the same 'Country'...
    MagentoSiteCountryFactory(
        magento_site=MagentoSiteFactory(),
        magento_country_id="BB",
        country=CountryFactory(iso2_code="BB"),
    )
    with pytest.raises(MagentoError) as e:
        MagentoSiteCountry.objects.find_country(magento_site, "BB")
    assert (
        "Invalid Magento Country ID: 'BB' (site '{}') (did you run the "
        "'magento_sync_settings' management command)?".format(magento_site.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_ordering():
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="CC",
        country=CountryFactory(iso2_code="CC"),
    )
    MagentoSiteCountryFactory(
        magento_site=MagentoSiteFactory(),
        magento_country_id="BB",
        country=CountryFactory(iso2_code="BB"),
    )
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="AA",
        country=CountryFactory(iso2_code="AA"),
    )
    assert ["AA", "CC", "BB"] == [
        x.magento_country_id for x in MagentoSiteCountry.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    magento_site = MagentoSiteFactory()
    x = MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="AD",
        country=CountryFactory(iso2_code="AD", name="Andorra"),
        # name="Andorra",
        # iso2_code="AD",
        # iso3_code="AND",
    )
    assert "AD Andorra (site {})".format(magento_site.pk) == str(x)


@pytest.mark.django_db
def test_unique_together():
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site, magento_country_id="AA"
    )
    with pytest.raises(IntegrityError) as e:
        MagentoSiteCountryFactory(
            magento_site=magento_site, magento_country_id="AA"
        )
    assert "duplicate key value violates unique constraint" in str(e.value)
