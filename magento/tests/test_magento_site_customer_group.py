# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from magento.models import MagentoError, MagentoSiteCustomerGroup
from .factories import MagentoSiteFactory, MagentoSiteCustomerGroupFactory


@pytest.mark.django_db
def test_get_by_customer_group_id():
    """Find the customer group using the Magento customer group ID."""
    magento_site = MagentoSiteFactory()
    x = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=45
    )
    assert x == MagentoSiteCustomerGroup.objects.get_by_customer_group_id(
        magento_site, 45
    )


@pytest.mark.django_db
def test_get_by_customer_group_id_does_not_exist():
    """Find the customer group using the Magento customer group ID."""
    magento_site = MagentoSiteFactory()
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=45
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=MagentoSiteFactory(), magento_customer_group_id=46
    )
    with pytest.raises(MagentoError) as e:
        MagentoSiteCustomerGroup.objects.get_by_customer_group_id(
            magento_site, 46
        )
    assert (
        "Invalid Magento Group ID: '46' (site '{}') (did you run the "
        "'magento_sync_settings' management command)?".format(magento_site.pk)
    ) in str(e.value)


# @pytest.mark.django_db
# def test_group_id_on_account():
#    """Check the Magento settings to see if we post invoices on-account."""
#    magento_site = MagentoSiteFactory()
#    x = MagentoSiteCustomerGroupFactory(
#        magento_site=magento_site, magento_customer_group_id=45
#    )
#    assert (
#        MagentoSiteCustomerGroup.objects.group_id_on_account(magento_site, 45)
#        is True
#    )


@pytest.mark.django_db
def test_ordering():
    magento_site = MagentoSiteFactory()
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site,
        magento_customer_group_id=45,
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site,
        magento_customer_group_id=46,
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=MagentoSiteFactory(),
        magento_customer_group_id=45,
    )
    assert [45, 46, 45] == [
        x.magento_customer_group_id
        for x in MagentoSiteCustomerGroup.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    magento_site = MagentoSiteFactory()
    x = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=46, code="Apple"
    )
    assert "Apple".format(magento_site.pk) == str(x)


@pytest.mark.django_db
def test_unique_together():
    magento_site = MagentoSiteFactory()
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site,
        magento_customer_group_id=46,
    )
    with pytest.raises(IntegrityError) as e:
        MagentoSiteCustomerGroupFactory(
            magento_site=magento_site,
            magento_customer_group_id=46,
        )
    assert "duplicate key value violates unique constraint" in str(e.value)
