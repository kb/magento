# -*- encoding: utf-8 -*-
import pytest

from invoice.tests.factories import PaymentProcessorFactory
from magento.models import MagentoSitePaymentMethod
from .factories import MagentoSiteFactory, MagentoSitePaymentMethodFactory


@pytest.mark.django_db
def test_current():
    magento_site = MagentoSiteFactory()
    MagentoSitePaymentMethodFactory(slug="a", magento_site=magento_site)
    MagentoSitePaymentMethodFactory(slug="b", magento_site=MagentoSiteFactory())
    MagentoSitePaymentMethodFactory(slug="c", magento_site=magento_site)
    assert ["a", "c"] == [
        x.slug for x in MagentoSitePaymentMethod.objects.current(magento_site)
    ]


@pytest.mark.django_db
def test_init_magento_site_payment_method():
    magento_site = MagentoSiteFactory()
    x = MagentoSitePaymentMethod.objects.init_magento_site_payment_method(
        magento_site, "apple", "Orange"
    )
    assert x.on_account is False
    assert "apple" == x.slug
    assert magento_site == x.magento_site
    assert "Orange" == x.payment_processor.description


@pytest.mark.django_db
def test_init_magento_site_payment_method_duplicate():
    magento_site = MagentoSiteFactory()
    m1 = MagentoSitePaymentMethod.objects.init_magento_site_payment_method(
        magento_site, "satsuma", "Orange"
    )
    m1.on_account = True
    m1.save()
    # description will not change because payment processor is already created
    m2 = MagentoSitePaymentMethod.objects.init_magento_site_payment_method(
        magento_site, "satsuma", "Banana"
    )
    assert 1 == MagentoSitePaymentMethod.objects.count()
    assert magento_site == m2.magento_site
    assert "satsuma" == m2.slug
    assert "Orange" == m2.payment_processor.description
    assert m2.on_account is True


@pytest.mark.django_db
def test_ordering():
    MagentoSitePaymentMethodFactory(slug="B")
    MagentoSitePaymentMethodFactory(slug="A")
    assert ["A", "B"] == [
        x.slug for x in MagentoSitePaymentMethod.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    magento_site = MagentoSiteFactory(description="Live")
    x = MagentoSitePaymentMethodFactory(
        magento_site=magento_site,
        slug="apple",
        payment_processor=PaymentProcessorFactory(description="Orange"),
    )
    assert "Orange for Live (apple)" == str(x)
