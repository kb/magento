# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.core.management import call_command
from django.core.management.base import CommandError
from login.tests.factories import UserFactory
from unittest import mock

from finance.tests.factories import CountryFactory
from invoice.models import Invoice, InvoiceCredit, InvoiceIssue
from invoice.tests.factories import InvoiceFactory
from magento.models import INVOICE_PREFIX, MagentoSettings
from magento.tests.factories import (
    MagentoSiteCountryFactory,
    MagentoSiteCustomerGroupFactory,
)
from stock.tests.factories import ProductFactory
from .factories import (
    MagentoInvoiceFactory,
    MagentoSettingsFactory,
    MagentoSiteFactory,
)


@pytest.mark.django_db
def test_demo_data_magento():
    call_command("demo_data_magento")
    magento_settings = MagentoSettings.load()
    assert "missing" == magento_settings.missing_product.slug
    assert "shipping" == magento_settings.shipping_product.slug


@pytest.mark.django_db
def test_download_magento_credit_note():
    country = CountryFactory(iso2_code="GB")
    invoice = InvoiceFactory(number=1111, prefix=INVOICE_PREFIX)
    magento_site = MagentoSiteFactory()
    MagentoInvoiceFactory(invoice=invoice, magento_invoice_id=123)
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=country,
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        "magento.service.MagentoClient.customer"
    ) as mock_customer, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo.return_value = {
            "base_currency_code": "red",
            "base_discount_amount": "0.0000",
            "base_grand_total": "132",
            "base_shipping_amount": 10,
            "base_shipping_tax_amount": 2,
            "base_tax_amount": "22.0000",
            "base_to_global_rate": "1.0000",
            "created_at": "2018-01-23 10:33:01",
            "increment_id": 8888,
            "invoice_id": 123,
            "order_increment_id": 789,
            "items": [
                {
                    "base_discount_amount": 0,
                    "base_price": 100,
                    "base_row_total": 100,
                    "base_tax_amount": 20,
                    "description": "Colours",
                    "name": "Colour",
                    "qty": "1",
                    "sku": "green",
                }
            ],
        }
        mock_customer.return_value = {"customer_id": 4545, "group_id": 4646}
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": 414141,
            "customer_taxvat": "ABC",
            "increment_id": "789",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.filter(is_credit=True).count()
        assert 0 == InvoiceCredit.objects.count()
        assert 0 == InvoiceIssue.objects.count()
        call_command("download_magento_credit_note", 123)
    qs = Invoice.objects.filter(is_credit=True)
    assert 1 == qs.count()
    credit_note = qs.first()
    assert Decimal("132") == credit_note.gross
    assert 0 == InvoiceIssue.objects.count()
    assert 1 == InvoiceCredit.objects.count()
    invoice_credit = InvoiceCredit.objects.first()
    assert credit_note == invoice_credit.credit_note
    assert invoice == invoice_credit.invoice


@pytest.mark.django_db
def test_download_magento_credit_note_no_settings():
    with pytest.raises(CommandError) as e:
        call_command("download_magento_credit_note", 123)
    assert "can only work with a single Magento site" in str(e.value)


@pytest.mark.django_db
def test_download_magento_invoice():
    country = CountryFactory(iso2_code="GB")
    magento_site = MagentoSiteFactory()
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=country,
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.customer"
    ) as mock_customer, mock.patch(
        "magento.service.MagentoClient.invoice_id"
    ) as mock_invoice_id, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_customer.return_value = {"customer_id": 4545, "group_id": 4646}
        mock_invoice_id.return_value = {
            "base_currency_code": "red",
            "base_discount_amount": 0,
            "base_grand_total": "132",
            "base_shipping_amount": 10,
            "base_shipping_tax_amount": 2,
            "base_tax_amount": 22,
            "base_to_global_rate": "1.0000",
            "created_at": "2018-01-23 10:33:01",
            "increment_id": 134985,
            "invoice_id": 123,
            "order_increment_id": 789,
            "items": [
                {
                    "base_discount_amount": 0,
                    "base_price": 50,
                    "base_row_total": 100,
                    "base_tax_amount": 20,
                    "description": "Colours",
                    "name": "Colour",
                    "qty": "2",
                    "sku": "green",
                }
            ],
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": 414141,
            "customer_taxvat": "ABC",
            "increment_id": "789",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        call_command("download_magento_invoice", 134985)
    assert 1 == Invoice.objects.count()
    invoice = Invoice.objects.first()
    assert "I134985" == invoice.invoice_number
    assert Decimal("132") == invoice.gross
    assert ["green", "shipping"] == [
        x.product.slug for x in invoice.invoiceline_set.all()
    ]
    assert 0 == InvoiceIssue.objects.count()


@pytest.mark.django_db
def test_download_magento_invoice_no_settings():
    with pytest.raises(CommandError) as e:
        call_command("download_magento_invoice", 123)
    assert "can only work with a single Magento site" in str(e.value)


@pytest.mark.django_db
def test_init_app_contact():
    call_command("init_app_magento")


@pytest.mark.django_db
def test_magento_sync_settings():
    call_command("magento_sync_settings")
