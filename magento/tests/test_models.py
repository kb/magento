# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from freezegun import freeze_time
from unittest import mock

from contact.tests.factories import ContactFactory, ContactAddress
from finance.models import Currency, FinanceError, VatCode
from finance.tests.factories import (
    CountryFactory,
    CurrencyFactory,
    VatSettingsFactory,
)
from invoice.models import Invoice, InvoiceContact, InvoiceCredit, InvoiceIssue
from invoice.tests.factories import InvoiceContactFactory, InvoiceFactory
from login.tests.factories import UserFactory
from magento.models import (
    create_or_find_contact,
    create_or_find_credit_note,
    create_or_find_invoice,
    create_or_find_user,
    CREDIT_NOTE_PREFIX,
    download_credit_notes,
    download_invoices,
    find_vat_code,
    INVOICE_PREFIX,
    MagentoError,
    MagentoInvoice,
    MagentoSiteCountry,
    MagentoSiteCustomerGroup,
    sync_magento,
    SyncSettings,
)
from magento.service import (
    MagentoAddressData,
    MagentoCustomerData,
    MagentoInvoiceData,
    MagentoOrderData,
    MagentoClient,
)
from stock.tests.factories import ProductFactory
from .example_variables import (
    example_invoice_full,
    example_invoice_full_eur,
    example_order,
)
from .factories import (
    MagentoInvoiceFactory,
    MagentoSitePaymentMethodFactory,
    MagentoSettingsFactory,
    MagentoSiteCountryFactory,
    MagentoSiteCustomerGroupFactory,
    MagentoSiteFactory,
)


@pytest.mark.django_db
def test_create_or_find_user_with_find():
    user = UserFactory(username="test@test.com")
    assert user == create_or_find_user("test@test.com", None, None)


@pytest.mark.django_db
def test_create_or_find_user_with_create():
    user = create_or_find_user("test@test.com", "test1", "test2")
    assert "test@test.com" == user.username
    assert "test1" == user.first_name
    assert "test2" == user.last_name
    assert user.is_active is False
    assert user.is_staff is False
    assert user.has_usable_password() is False


@pytest.mark.django_db
def test_create_or_find_contact_with_find_and_no_company():
    user = UserFactory(username="test@test.com")
    contact = ContactFactory(user=user)
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="44",
        country=CountryFactory(iso2_code="FR"),
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    assert contact == create_or_find_contact(
        user,
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="",
                address_two="",
                town="",
                postal_code="",
                admin_area="",
                country_id="44",
            ),
            company="",
            customer_address_id="",
            customer_id=414141,
            email="",
            first_name="",
            last_name="",
            order_id="",
            order_increment_id="",
            paid="",
            payment_method="",
            phone="",
            vat_number="",
        ),
        MagentoCustomerData(customer_id=414141, group_id=4646),
        magento_site,
    )


@pytest.mark.django_db
def test_create_or_find_contact_with_find_and_company():
    user = UserFactory(username="test@test.com")
    contact = ContactFactory(user=user, company_name="test")
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="44",
        country=CountryFactory(iso2_code="FR"),
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    assert contact == create_or_find_contact(
        user,
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="",
                address_two="",
                town="",
                postal_code="",
                admin_area="",
                country_id="44",
            ),
            company="",
            customer_address_id="",
            customer_id=414141,
            email="",
            first_name="",
            last_name="",
            order_id="",
            order_increment_id="",
            paid="",
            payment_method="",
            phone="",
            vat_number="",
        ),
        MagentoCustomerData(customer_id=414141, group_id=4646),
        magento_site,
    )
    invoice_contact = InvoiceContact.objects.get(contact=contact)
    assert invoice_contact.on_account is False


@pytest.mark.django_db
def test_create_or_find_contact_with_create_and_no_company():
    country = CountryFactory(iso2_code="FR")
    user = UserFactory(username="test@test.com")
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site, magento_country_id="44", country=country
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    contact = create_or_find_contact(
        user,
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="",
                address_two="",
                town="",
                postal_code="",
                admin_area="",
                country_id="44",
            ),
            company="",
            customer_address_id="",
            customer_id=None,
            email="",
            first_name="",
            last_name="",
            order_id="",
            order_increment_id="",
            paid="",
            payment_method="",
            phone="",
            vat_number="",
        ),
        MagentoCustomerData(customer_id=414141, group_id=4646),
        magento_site,
    )
    assert contact.company_name == ""
    assert contact.user == user
    assert country == contact.invoicecontact.country
    invoice_contact = InvoiceContact.objects.get(contact=contact)
    assert invoice_contact.on_account is False


@pytest.mark.django_db
def test_create_or_find_contact_with_create_and_company():
    user = UserFactory(username="test@test.com")
    magento_site = MagentoSiteFactory()
    # this customer_group is on account
    customer_group = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    magento_site.on_account_customer_groups.add(customer_group)
    # country
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="44",
        country=CountryFactory(iso2_code="FR"),
    )
    contact = create_or_find_contact(
        user,
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="",
                address_two="",
                town="",
                postal_code="",
                admin_area="",
                country_id="44",
            ),
            company="KB",
            customer_address_id="",
            customer_id=414141,
            email="",
            first_name="",
            last_name="",
            order_id="",
            order_increment_id="",
            paid="",
            payment_method="",
            phone="",
            vat_number="",
        ),
        MagentoCustomerData(customer_id=414141, group_id=4646),
        magento_site,
    )
    assert "KB" == contact.company_name
    assert contact.user == user
    # check the customer_group is on account
    invoice_contact = InvoiceContact.objects.get(contact=contact)
    assert invoice_contact.on_account is True


@pytest.mark.django_db
def test_create_or_find_contact_blank_address_items():
    user = UserFactory(username="test@test.com")
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="44",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    contact = create_or_find_contact(
        user,
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="",
                address_two="",
                town="",
                postal_code="",
                admin_area="",
                country_id="44",
            ),
            company="KB",
            customer_address_id="",
            customer_id=414141,
            email="",
            first_name="",
            last_name="",
            order_id="",
            order_increment_id="",
            paid="",
            payment_method="",
            phone="",
            vat_number="",
        ),
        MagentoCustomerData(customer_id=414141, group_id=4646),
        magento_site,
    )
    contact_address = ContactAddress.objects.get(contact=contact)
    assert contact_address.town == ""
    assert contact_address.country == "France"
    assert contact_address.postal_code == ""
    assert contact_address.admin_area == ""
    assert "France" == contact.invoicecontact.country.name


@pytest.mark.django_db
def test_create_or_find_contact_with_address_items():
    user = UserFactory(username="test@test.com")
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="jhi",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    contact = create_or_find_contact(
        user,
        MagentoOrderData(
            address=MagentoAddressData(
                address_one="",
                address_two="",
                town="abc",
                postal_code="def",
                admin_area="klm",
                country_id="jhi",
            ),
            company="KB",
            customer_address_id="",
            customer_id=414141,
            email="",
            first_name="",
            last_name="",
            order_id="",
            order_increment_id="",
            paid="",
            payment_method="",
            phone="",
            vat_number="",
        ),
        MagentoCustomerData(customer_id=414141, group_id=4646),
        magento_site,
    )
    contact_address = ContactAddress.objects.get(contact=contact)
    assert contact_address.town == "abc"
    assert contact_address.country == "France"
    assert contact_address.postal_code == "def"
    assert contact_address.admin_area == "klm"


@pytest.mark.parametrize(
    "transaction_date,expect",
    [
        (date(2019, 3, 21), "21/03/2019"),
        (date(2019, 5, 21), "21/05/2019"),
        (date(2020, 12, 21), "21/12/2020"),
    ],
)
@pytest.mark.django_db
def test_sync_magento_today(transaction_date, expect):
    """We should only download from Magento for days in the past."""
    with freeze_time("2019-03-21"):
        with pytest.raises(MagentoError) as e:
            sync_magento(transaction_date)
    assert (
        "Magento invoices can only be downloaded for dates in the "
        "past (i.e. {} must be before 21/03/2019)".format(expect)
    ) in str(e.value)


@pytest.mark.django_db
def test_sync_magento_yesterday():
    with freeze_time("2019-03-21"):
        count = sync_magento(date(2019, 3, 20))
    assert 0 == count


@pytest.mark.django_db
def test_create_or_find_invoice_with_find():
    system_user = UserFactory(username="system.generated")
    currency = CurrencyFactory()
    user = UserFactory(username="test@test.com")
    contact = ContactFactory(user=user)
    invoice = InvoiceFactory(
        user=system_user,
        contact=contact,
        currency=currency,
        exchange_rate=Decimal(),
        number="1",
        upfront_payment=True,
        prefix="I",
    )
    assert invoice == create_or_find_invoice(
        MagentoSiteFactory(),
        MagentoInvoiceData(
            base_grand_total=Decimal("120.00"),
            create_issue_messages=[],
            currency_code=currency.slug,
            exchange_rate=Decimal("1.00"),
            invoice_date=date(2018, 1, 1),
            invoice_id="9876",
            invoice_increment_id=1,
            is_credit=True,
            lines=[],
            order_increment_id=100000005,
            order=MagentoOrderData(
                address=MagentoAddressData(
                    address_one="",
                    address_two="",
                    town="",
                    postal_code="",
                    admin_area="",
                    country_id="",
                ),
                company="",
                customer_address_id="",
                customer_id=414141,
                email="",
                first_name="",
                last_name="",
                order_id="14",
                order_increment_id=323232,
                paid="",
                payment_method="",
                phone="",
                vat_number="",
            ),
        ),
        contact,
        "Apple Sauce",
    )


@pytest.mark.django_db
def test_create_or_find_invoice_with_create():
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    VatSettingsFactory(
        zero_rate_vat_code=VatCode.objects.get(slug=VatCode.ZERO_RATE)
    )
    UserFactory(username="system.generated")
    user = UserFactory(username="test@test.com")
    contact = ContactFactory(user=user)
    InvoiceContact(contact=contact)
    assert 0 == MagentoInvoice.objects.count()
    with mock.patch(
        "magento.service.MagentoClient.invoice_id"
    ) as mock_invoice_id, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_invoice_id.return_value = example_invoice_full
        mock_sales_order.return_value = example_order
        # prepare the data
        invoice_increment_id = 100000003
        magento = MagentoClient(MagentoSiteFactory())
        magento_invoice_data = magento.invoice_id_data(invoice_increment_id)
        magento_invoice_data.order = magento.sales_order_data(
            magento_invoice_data.order_increment_id
        )
    invoice = create_or_find_invoice(
        MagentoSiteFactory(), magento_invoice_data, contact, "Apple Sauce"
    )
    assert invoice.number == 100000003
    assert date(2018, 1, 18) == invoice.invoice_date
    assert invoice.contact == contact
    assert invoice.upfront_payment == True
    assert invoice.prefix == "I"
    assert invoice.contact == contact
    assert invoice.currency == Currency.objects.get(slug=Currency.POUND)
    assert invoice.exchange_rate == Decimal("1.000")
    assert invoice.source == "Apple Sauce"
    assert 1 == MagentoInvoice.objects.count()
    magento_invoice = MagentoInvoice.objects.first()
    assert invoice == magento_invoice.invoice
    assert 3 == magento_invoice.magento_invoice_id
    assert 100000003 == magento_invoice.magento_invoice_increment_id
    assert 14 == magento_invoice.magento_order_id
    assert 100019917 == magento_invoice.magento_order_increment_id
    assert ["zeni5", "shipping"] == [
        x.product.slug for x in invoice.invoiceline_set.all().order_by("pk")
    ]
    assert [Decimal("11.99"), Decimal("4.49")] == [
        x.net for x in invoice.invoiceline_set.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_create_or_find_invoice_with_create_eur():
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    UserFactory(username="system.generated")
    user = UserFactory(username="test@test.com")
    contact = ContactFactory(user=user)
    InvoiceContact(contact=contact)
    VatSettingsFactory(
        zero_rate_vat_code=VatCode.objects.get(slug=VatCode.ZERO_RATE)
    )
    with mock.patch(
        "magento.service.MagentoClient.invoice_id"
    ) as mock_invoice_id, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_invoice_id.return_value = example_invoice_full_eur
        mock_sales_order.return_value = example_order
        # prepare the data
        invoice_increment_id = 100018188
        magento = MagentoClient(MagentoSiteFactory())
        magento_invoice_data = magento.invoice_id_data(invoice_increment_id)
        magento_invoice_data.order = magento.sales_order_data(
            magento_invoice_data.order_increment_id
        )
    invoice = create_or_find_invoice(
        MagentoSiteFactory(), magento_invoice_data, contact, "Apple Sauce"
    )
    assert invoice.number == 100018188
    assert invoice.invoice_date == date(2018, 10, 4)
    assert invoice.contact == contact
    assert invoice.upfront_payment == True
    assert invoice.prefix == "I"
    assert invoice.contact == contact
    assert invoice.currency == Currency.objects.get(slug=Currency.POUND)
    assert invoice.source == "Apple Sauce"
    assert [
        "bergoeo100",
        "coconwro10000",
        "palmwo5000",
        "grapepieoow100",
        "lemgraeoo250",
        "litcuborg250",
        "peppoeo100",
        "sheaur10000",
        "shipping",
    ] == [x.product.slug for x in invoice.invoiceline_set.all().order_by("pk")]


@pytest.mark.django_db
def test_create_or_find_invoice_with_on_account():
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    magento_site = MagentoSiteFactory()
    MagentoSitePaymentMethodFactory(
        magento_site=magento_site, slug="bank_transfer", on_account=True
    )
    VatSettingsFactory(
        zero_rate_vat_code=VatCode.objects.get(slug=VatCode.ZERO_RATE)
    )
    UserFactory(username="system.generated")
    user = UserFactory(username="test@test.com")
    contact = ContactFactory(user=user)
    InvoiceContact(contact=contact)
    assert 0 == MagentoInvoice.objects.count()
    with mock.patch(
        "magento.service.MagentoClient.invoice_id"
    ) as mock_invoice_id, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_invoice_id.return_value = example_invoice_full
        # set the payment method to 'bank_transfer'
        x = example_order
        x["payment"]["method"] = "bank_transfer"
        mock_sales_order.return_value = x
        # prepare the data
        invoice_increment_id = 100000003
        magento = MagentoClient(magento_site)
        magento_invoice_data = magento.invoice_id_data(invoice_increment_id)
        magento_invoice_data.order = magento.sales_order_data(
            magento_invoice_data.order_increment_id
        )
    invoice = create_or_find_invoice(
        magento_site, magento_invoice_data, contact, "Apple Sauce"
    )
    assert invoice.number == 100000003
    assert date(2018, 1, 18) == invoice.invoice_date
    assert invoice.contact == contact
    assert invoice.upfront_payment == False
    assert invoice.upfront_payment_processor is None
    assert invoice.prefix == "I"
    assert invoice.contact == contact
    assert invoice.currency == Currency.objects.get(slug=Currency.POUND)
    assert invoice.exchange_rate == Decimal("1.000")
    assert invoice.source == "Apple Sauce"
    assert 1 == MagentoInvoice.objects.count()
    magento_invoice = MagentoInvoice.objects.first()
    assert invoice == magento_invoice.invoice
    assert 3 == magento_invoice.magento_invoice_id
    assert 100000003 == magento_invoice.magento_invoice_increment_id
    assert 14 == magento_invoice.magento_order_id
    assert 100019917 == magento_invoice.magento_order_increment_id
    assert ["zeni5", "shipping"] == [
        x.product.slug for x in invoice.invoiceline_set.all().order_by("pk")
    ]
    assert [Decimal("11.99"), Decimal("4.49")] == [
        x.net for x in invoice.invoiceline_set.all().order_by("pk")
    ]


@pytest.mark.django_db
def test_create_or_find_credit_note_with_create():
    user = UserFactory(username="system.generated")
    MagentoSettingsFactory()
    contact = ContactFactory(user=user)
    InvoiceContactFactory(contact=contact)
    currency = CurrencyFactory()
    create_or_find_credit_note(
        MagentoInvoiceData(
            base_grand_total=Decimal("120.00"),
            create_issue_messages=[],
            currency_code=currency.slug,
            exchange_rate=Decimal("1.00"),
            invoice_date=date(2018, 1, 18),
            invoice_id="9876",
            invoice_increment_id=1029,
            is_credit=True,
            lines=[],
            order_increment_id=100000005,
            order=MagentoOrderData(
                address=MagentoAddressData(
                    address_one="",
                    address_two="",
                    town="",
                    postal_code="",
                    admin_area="",
                    country_id="",
                ),
                company="",
                customer_address_id="",
                customer_id=414141,
                email="",
                first_name="",
                last_name="",
                order_id="14",
                order_increment_id=787878,
                paid="",
                payment_method="",
                phone="",
                vat_number="",
            ),
        ),
        contact,
        "Apple Sauce",
    )
    credit_note = Invoice.objects.get(number="1029", prefix="C")
    assert credit_note.number == 1029
    assert credit_note.invoice_date == date(2018, 1, 18)
    assert credit_note.contact == contact
    assert credit_note.currency == currency
    assert credit_note.prefix == "C"
    assert credit_note.source == "Apple Sauce"
    assert credit_note.is_draft == False
    assert credit_note.user == user


@pytest.mark.django_db
def test_create_or_find_credit_note_with_find():
    user = UserFactory(username="system.generated")
    contact = ContactFactory(user=user)
    currency = CurrencyFactory()
    credit_note = InvoiceFactory(
        user=user,
        number="1029",
        invoice_date=date(2018, 1, 18),
        contact=contact,
        currency=currency,
        prefix=CREDIT_NOTE_PREFIX,
        is_credit=True,
        is_draft=False,
    )
    credit_note.save()
    assert credit_note == create_or_find_credit_note(
        MagentoInvoiceData(
            base_grand_total=Decimal("120.00"),
            create_issue_messages=[],
            currency_code=currency.slug,
            exchange_rate=Decimal("1.00"),
            invoice_date=date(2018, 1, 18),
            invoice_id="9876",
            invoice_increment_id=1029,
            is_credit=True,
            lines=[],
            order_increment_id="100000005",
            order=MagentoOrderData(
                address=MagentoAddressData(
                    address_one="",
                    address_two="",
                    town="",
                    postal_code="",
                    admin_area="",
                    country_id="",
                ),
                company="",
                customer_address_id="",
                customer_id=414141,
                email="",
                first_name="",
                last_name="",
                order_id="14",
                order_increment_id=787878,
                paid="",
                payment_method="",
                phone="",
                vat_number="",
            ),
        ),
        contact,
        "Apple Sauce",
    )


@pytest.mark.django_db
def test_download_credit_notes():
    invoice = InvoiceFactory(number=1111, prefix=INVOICE_PREFIX)
    MagentoInvoiceFactory(invoice=invoice, magento_invoice_id=123)
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4646
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo_list_filters"
    ) as mock_credit_memo_list_filters, mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        "magento.service.MagentoClient.customer"
    ) as mock_customer, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo_list_filters.return_value = [
            {"created_at": "2017-12-08", "increment_id": 134985}
        ]
        mock_credit_memo.return_value = {
            "base_currency_code": "red",
            "base_discount_amount": "0.0000",
            "base_grand_total": "132",
            "base_shipping_amount": 10,
            "base_shipping_tax_amount": 2,
            "base_tax_amount": "22.0000",
            "base_to_global_rate": "1.000",
            "created_at": "2018-01-23 10:33:01",
            "increment_id": 8888,
            "invoice_id": 123,
            "order_increment_id": 44444444,
            "items": [
                {
                    "base_discount_amount": 0,
                    "base_price": 100,
                    "base_row_total": 100,
                    "base_tax_amount": 20,
                    "description": "Colours",
                    "name": "Colour",
                    "qty": "1",
                    "sku": "green",
                }
            ],
        }
        mock_customer.return_value = {"customer_id": 4545, "group_id": 4646}
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": "414141",
            "customer_taxvat": "ABC",
            "increment_id": "44444444",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.filter(is_credit=True).count()
        assert 0 == InvoiceCredit.objects.count()
        assert 0 == InvoiceIssue.objects.count()
        download_credit_notes(magento_site, date(2018, 4, 23))
    qs = Invoice.objects.filter(is_credit=True)
    assert 1 == qs.count()
    credit_note = qs.first()
    assert Decimal("132") == credit_note.gross
    assert Decimal("1.000") == credit_note.exchange_rate
    # lines
    assert credit_note.has_lines is True
    assert set(["shipping", "green"]) == set(
        [x.product.slug for x in credit_note.invoiceline_set.all()]
    )
    # no issues
    assert 0 == InvoiceIssue.objects.count()
    # link to invoice
    assert 1 == InvoiceCredit.objects.count()
    invoice_credit = InvoiceCredit.objects.first()
    assert credit_note == invoice_credit.credit_note
    assert invoice == invoice_credit.invoice
    # check the contact
    assert 1 == InvoiceContact.objects.count()
    invoice_contact = InvoiceContact.objects.first()
    assert "KB" == invoice_contact.contact.company_name
    assert "ABC" == invoice_contact.vat_number


@pytest.mark.django_db
def test_download_credit_notes_cannot_find_invoice():
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="orange"))
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo_list_filters"
    ) as mock_credit_memo_list_filters, mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        # "magento.service.MagentoClient.customer"
        # ) as mock_customer, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo_list_filters.return_value = [
            {"created_at": "2017-12-08", "increment_id": 134985}
        ]
        mock_credit_memo.return_value = {
            "base_currency_code": "red",
            "base_discount_amount": 0,
            "base_grand_total": "132",
            "base_shipping_amount": 10,
            "base_shipping_tax_amount": 2,
            "base_tax_amount": 22,
            "base_to_global_rate": "1.000",
            "created_at": "2018-01-18 13:38:45",
            "increment_id": 8888,
            "invoice_id": 123,
            "order_increment_id": "100005555",
            "items": [
                {
                    "base_discount_amount": 0,
                    "base_price": 50,
                    "base_row_total": 100,
                    "base_tax_amount": 20,
                    "description": "Colours",
                    "name": "Colour",
                    "qty": "2",
                    "sku": "green",
                }
            ],
        }
        # mock_customer.return_value = {"customer_id": 4545, "group_id": 4646}
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": None,
            "customer_taxvat": "ABC",
            "increment_id": "100005555",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.count()
        assert 0 == InvoiceCredit.objects.count()
        assert 0 == InvoiceIssue.objects.count()
        download_credit_notes(magento_site, date(2018, 4, 23))
    assert 1 == Invoice.objects.count()
    credit_note = Invoice.objects.first()
    assert Decimal("132") == credit_note.gross
    assert 0 == InvoiceCredit.objects.count()
    assert 1 == InvoiceIssue.objects.count()
    invoice_issue = InvoiceIssue.objects.first()
    assert credit_note == invoice_issue.invoice
    assert invoice_issue.confirmed is False
    assert invoice_issue.confirmed_by_user is None
    assert invoice_issue.comment is None
    assert ["Cannot find invoice for credit note"] == invoice_issue.lines()


@pytest.mark.django_db
def test_download_credit_notes_not_linked_to_an_invoice():
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="shipping"))
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo_list_filters"
    ) as mock_credit_memo_list_filters, mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo_list_filters.return_value = [
            {"created_at": "2017-12-08", "increment_id": 134985}
        ]
        mock_credit_memo.return_value = {
            "base_currency_code": "red",
            "base_discount_amount": 0,
            "base_grand_total": "132",
            "base_shipping_amount": 10,
            "base_shipping_tax_amount": 2,
            "base_tax_amount": 22,
            "base_to_global_rate": "1.000",
            "created_at": "2018-01-23 10:33:01",
            "increment_id": 8888,
            "invoice_id": 0,
            "items": [
                {
                    "base_discount_amount": 0,
                    "base_price": 50,
                    "base_row_total": 100,
                    "base_tax_amount": 20,
                    "description": "Colours",
                    "name": "Colour",
                    "qty": "2",
                    "sku": "green",
                }
            ],
            "order_increment_id": "100005555",
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": None,
            "customer_taxvat": "ABC",
            "increment_id": "100005555",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.count()
        assert 0 == InvoiceCredit.objects.count()
        assert 0 == InvoiceIssue.objects.count()
        download_credit_notes(magento_site, date(2018, 4, 23))
    assert 1 == Invoice.objects.count()
    credit_note = Invoice.objects.first()
    assert Decimal("132") == credit_note.gross
    assert 0 == InvoiceCredit.objects.count()
    assert 1 == InvoiceIssue.objects.count()
    invoice_issue = InvoiceIssue.objects.first()
    assert credit_note == invoice_issue.invoice
    assert invoice_issue.confirmed is False
    assert invoice_issue.confirmed_by_user is None
    assert invoice_issue.comment is None
    assert ["Not linked to an invoice"] == invoice_issue.lines()


@pytest.mark.django_db
def test_download_invoices():
    MagentoSettingsFactory(shipping_product=ProductFactory(slug="orange"))
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.invoice_list"
    ) as mock_invoice_list, mock.patch(
        "magento.service.MagentoClient.invoice_id"
    ) as mock_invoice_id, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_invoice_list.return_value = [
            {"created_at": "2017-12-08", "increment_id": 134985}
        ]
        mock_invoice_id.return_value = {
            "base_currency_code": "red",
            "base_discount_amount": 0,
            "base_grand_total": "132",
            "base_shipping_amount": 10,
            "base_shipping_tax_amount": 2,
            "base_tax_amount": 22,
            "base_to_global_rate": "1.000",
            "created_at": "2018-01-23 10:33:01",
            "increment_id": 134985,
            "invoice_id": 123,
            "order_increment_id": "100005555",
            "items": [
                {
                    "base_discount_amount": 0,
                    "base_price": 50,
                    "base_row_total": 100,
                    "base_tax_amount": 20,
                    "description": "Colours",
                    "name": "Colour",
                    "qty": "2",
                    "sku": "green",
                }
            ],
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": None,
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": None,
            "customer_taxvat": "ABC",
            "increment_id": "100005555",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.count()
        assert 0 == InvoiceIssue.objects.count()
        download_invoices(magento_site, date(2018, 4, 23))
    assert 1 == Invoice.objects.count()
    invoice = Invoice.objects.first()
    assert Decimal("132") == invoice.gross
    assert 0 == InvoiceIssue.objects.count()
    assert 1 == InvoiceContact.objects.count()
    # handle a 'region' of 'None'
    address = invoice.contact.current_address()
    assert "" == address.admin_area
    invoice_contact = InvoiceContact.objects.first()
    assert "KB" == invoice_contact.contact.company_name
    assert "ABC" == invoice_contact.vat_number
    assert invoice_contact.on_account is False


@pytest.mark.parametrize(
    "rate,expect",
    [
        (Decimal("0.20"), "S"),
        (Decimal("0.19"), "S"),
        (Decimal("0.18"), "S"),
        (Decimal("0.17"), "S"),
        (Decimal("0.16"), "S"),
        (Decimal("0.05"), "S"),
        (Decimal("0.00"), "Z"),
        (0.20, "S"),
    ],
)
@pytest.mark.django_db
def test_find_vat_code(rate, expect):
    invoice_contact = InvoiceContactFactory(contact=ContactFactory())
    VatSettingsFactory(
        zero_rate_vat_code=VatCode.objects.get(slug=VatCode.ZERO_RATE)
    )
    vat_code = find_vat_code(invoice_contact, rate)
    assert expect == vat_code.slug


@pytest.mark.django_db
def test_find_vat_code_empty():
    contact = ContactFactory(company_name="KB")
    invoice_contact = InvoiceContactFactory(
        contact=contact,
        country=CountryFactory(iso2_code="GB", name="UK"),
        vat_number="123",
    )
    vat_settings = VatSettingsFactory(zero_rate_vat_code=None)
    vat_settings.european_union_countries.add(CountryFactory(iso2_code="FR"))
    with pytest.raises(MagentoError) as e:
        find_vat_code(invoice_contact, Decimal())
    assert (
        "Cannot find a Zero Rate VAT code for KB ({}) "
        "(VAT rate 0, Country GB, VAT Number 123)".format(contact.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_find_vat_code_eu_country():
    country = CountryFactory(iso2_code="FR")
    invoice_contact = InvoiceContactFactory(
        contact=ContactFactory(), country=country, vat_number="123"
    )
    vat_settings = VatSettingsFactory(
        zero_rate_ec_vat_code=VatCode.objects.get(slug=VatCode.ZERO_RATE_EC)
    )
    vat_settings.european_union_countries.add(country)
    assert VatCode.objects.get(slug=VatCode.ZERO_RATE_EC) == find_vat_code(
        invoice_contact, Decimal()
    )


@pytest.mark.django_db
def test_find_vat_code_no_eu_countries():
    invoice_contact = InvoiceContactFactory(
        contact=ContactFactory(), country=CountryFactory(), vat_number="123"
    )
    VatSettingsFactory()
    with pytest.raises(FinanceError) as e:
        find_vat_code(invoice_contact, Decimal())
    assert (
        "There are no European Union Countries in the database (did "
        "you run the 'magento_sync_settings' management command)?"
    ) in str(e.value)


@pytest.mark.django_db
def test_sync_settings():
    magento_site = MagentoSiteFactory()
    sync_settings = SyncSettings()
    with mock.patch(
        "magento.service.MagentoClient.country_list"
    ) as mock_country_list, mock.patch(
        "magento.service.MagentoClient.customer_group_list"
    ) as mock_customer_group_list:
        mock_country_list.return_value = [
            {
                "country_id": "AD",
                "iso2_code": "AD",
                "iso3_code": "AND",
                "name": "Andorra",
            },
            {
                "country_id": "AE",
                "iso2_code": "AE",
                "iso3_code": "ARE",
                "name": "United Arab Emirates",
            },
        ]
        mock_customer_group_list.return_value = [
            {"customer_group_id": "6", "customer_group_code": "Regular"},
            {"customer_group_id": "5", "customer_group_code": "Trade"},
        ]
        sync_settings.sync()
        customer_group_1 = MagentoSiteCustomerGroup.objects.get(
            magento_customer_group_id=5
        )
        customer_group_2 = MagentoSiteCustomerGroup.objects.get(
            magento_customer_group_id=6
        )
    assert ["AD", "AE"] == [
        x.magento_country_id
        for x in MagentoSiteCountry.objects.current(magento_site)
    ]
    assert [("AD", "Andorra"), ("AE", "United Arab Emirates")] == [
        (x.country.iso2_code, x.country.name)
        for x in MagentoSiteCountry.objects.current(magento_site)
    ]
    assert [(5, "Trade"), (6, "Regular")] == [
        (x.magento_customer_group_id, x.code)
        for x in MagentoSiteCustomerGroup.objects.current(magento_site)
    ]
