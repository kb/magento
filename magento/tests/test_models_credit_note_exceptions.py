# -*- encoding: utf-8 -*-
"""
Prove fix for issues found on the live system...
"""
import pytest

from datetime import date
from decimal import Decimal
from unittest import mock

from finance.tests.factories import CountryFactory
from invoice.models import Invoice, InvoiceIssue
from login.tests.factories import UserFactory
from magento.models import download_credit_notes
from stock.tests.factories import ProductFactory
from .factories import (
    MagentoSettingsFactory,
    MagentoSiteCountryFactory,
    MagentoSiteFactory,
)


@pytest.mark.django_db
def test_download_credit_notes_base_shipping_incl_tax():
    """Credit note C100001906 does not reconcile, gross vs Magento total.

    https://www.kbsoftware.co.uk/crm/ticket/4181/

    Original error::

      Credit note C100001906:
      gross amount (8.32) does not equal Magento total (3.8400)

    ipdb> base_shipping_incl_tax
    Decimal('5.9900')
    ipdb> base_grand_total
    Decimal('3.8400')

    To solve the issue, we no longer use ``base_shipping_incl_tax``.
    We use ``base_shipping_amount`` and ``base_shipping_tax_amount`` instead.

    """
    product = ProductFactory(slug="orange")
    MagentoSettingsFactory(shipping_product=product)
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=CountryFactory(iso2_code="FR", name="France"),
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo_list_filters"
    ) as mock_credit_memo_list_filters, mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo_list_filters.return_value = [
            {
                "created_at": "2017-12-08",
                "increment_id": 134985,
                "invoice_id": 0,
            }
        ]
        mock_credit_memo.return_value = {
            "store_id": "1",
            "adjustment_positive": "0.0000",
            "base_shipping_tax_amount": "0.6400",
            "store_to_order_rate": "1.0000",
            "base_discount_amount": "0.0000",
            "base_to_order_rate": "1.0000",
            "grand_total": "3.8400",
            "base_adjustment_negative": "0.0000",
            "base_subtotal_incl_tax": "0.0000",
            "shipping_amount": "3.2000",
            "subtotal_incl_tax": "0.0000",
            "adjustment_negative": "0.0000",
            "base_shipping_amount": "3.2000",
            "store_to_base_rate": "1.0000",
            "base_to_global_rate": "1.0000",
            "base_adjustment": "0.0000",
            "base_subtotal": "0.0000",
            "discount_amount": "0.0000",
            "subtotal": "0.0000",
            "adjustment": "0.0000",
            "base_grand_total": "3.8400",
            "base_adjustment_positive": "0.0000",
            "base_tax_amount": "0.6400",
            "shipping_tax_amount": "0.6400",
            "tax_amount": "0.6400",
            "order_id": "38224",
            "email_sent": None,
            "creditmemo_status": None,
            "state": "2",
            "shipping_address_id": "76447",
            "billing_address_id": "76446",
            "invoice_id": "36517",
            "store_currency_code": "GBP",
            "order_currency_code": "GBP",
            "base_currency_code": "GBP",
            "global_currency_code": "GBP",
            "transaction_id": "3JU10380XR765140N",
            "increment_id": "100001906",
            "created_at": "2019-04-29 09:20:37",
            "updated_at": "2019-04-29 09:20:37",
            "hidden_tax_amount": "0.0000",
            "base_hidden_tax_amount": "0.0000",
            "shipping_hidden_tax_amount": None,
            "base_shipping_hidden_tax_amnt": None,
            "shipping_incl_tax": "5.9900",
            "base_shipping_incl_tax": "5.9900",
            "discount_description": None,
            "creditmemo_id": "1961",
            "order_increment_id": "100039538",
            "items": [],
            "comments": [],
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": None,
            "customer_taxvat": "ABC",
            "increment_id": "100039538",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.filter(is_credit=True).count()
        download_credit_notes(magento_site, date(2018, 4, 23))
        qs = Invoice.objects.filter(is_credit=True)
        assert 1 == qs.count()
        credit_note = qs.first()
        assert Decimal("3.84") == credit_note.gross
        assert credit_note.has_lines is True
        assert 1 == credit_note.invoiceline_set.count()
        credit_note_line = credit_note.invoiceline_set.first()
        assert product == credit_note_line.product


@pytest.mark.django_db
def test_download_credit_notes_shipping_only_credit_note():
    """Shipping only credit note.

    Original error::

      Credit note C100001804:
      gross amount (46.78) does not equal Magento total (21.5900)

    """
    product = ProductFactory(slug="orange")
    MagentoSettingsFactory(shipping_product=product)
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="22",
        country=CountryFactory(iso2_code="FR"),
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo_list_filters"
    ) as mock_credit_memo_list_filters, mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo_list_filters.return_value = [
            {
                "created_at": "2017-12-08",
                "increment_id": 134985,
                "invoice_id": 0,
            }
        ]
        mock_credit_memo.return_value = {
            "store_id": "1",
            "adjustment_positive": "0.0000",
            "base_shipping_tax_amount": "3.6000",
            "store_to_order_rate": "1.0000",
            "base_discount_amount": "0.0000",
            "base_to_order_rate": "1.0000",
            "grand_total": "21.5900",
            "base_adjustment_negative": "0.0000",
            "base_subtotal_incl_tax": "0.0000",
            "shipping_amount": "17.9900",
            "subtotal_incl_tax": "0.0000",
            "adjustment_negative": "0.0000",
            "base_shipping_amount": "17.9900",
            "store_to_base_rate": "1.0000",
            "base_to_global_rate": "1.0000",
            "base_adjustment": "0.0000",
            "base_subtotal": "0.0000",
            "discount_amount": "0.0000",
            "subtotal": "0.0000",
            "adjustment": "0.0000",
            "base_grand_total": "21.5900",
            "base_adjustment_positive": "0.0000",
            "base_tax_amount": "3.6000",
            "shipping_tax_amount": "3.6000",
            "tax_amount": "3.6000",
            "order_id": "36326",
            "email_sent": None,
            "creditmemo_status": None,
            "state": "2",
            "shipping_address_id": "72651",
            "billing_address_id": "72650",
            "invoice_id": "34706",
            "store_currency_code": "GBP",
            "order_currency_code": "GBP",
            "base_currency_code": "GBP",
            "global_currency_code": "GBP",
            "transaction_id": None,
            "increment_id": "100001804",
            "created_at": "2019-04-01 10:11:05",
            "updated_at": "2019-04-01 10:11:05",
            "hidden_tax_amount": "0.0000",
            "base_hidden_tax_amount": "0.0000",
            "shipping_hidden_tax_amount": None,
            "base_shipping_hidden_tax_amnt": None,
            "shipping_incl_tax": "21.5900",
            "base_shipping_incl_tax": "21.5900",
            "discount_description": None,
            "creditmemo_id": "1857",
            "order_increment_id": "100037588",
            "items": [],
            "comments": [],
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": None,
            "customer_taxvat": "ABC",
            "increment_id": "100037588",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.filter(is_credit=True).count()
        download_credit_notes(magento_site, date(2018, 4, 23))
        qs = Invoice.objects.filter(is_credit=True)
        assert 1 == qs.count()
        credit_note = qs.first()
        assert Decimal("21.59") == credit_note.gross
        assert credit_note.has_lines is True
        assert 1 == credit_note.invoiceline_set.count()
        credit_note_line = credit_note.invoiceline_set.first()
        assert product == credit_note_line.product


@pytest.mark.django_db
def test_download_credit_notes_does_not_balance():
    """
    ERROR [magento.models:990]

    Credit note C100001920: gross amount (34.79)
            does not equal Magento total (37.7300)

    The credit note has one line::

      net       vat     total
      34.00     6.80    40.80

    The shipping is a negative value::

      net       vat     total
      -5.01     -1,00   -6.01

    Can our credit notes handle negative lines?
    It feels as if the ``net`` value will need to be = / - for this to work
    because I can't multiply by the quantity to get the total.

    .. note:: This test uses the ``_create_adjustment_lines``
              and creates a ``missing`` line.

    """
    MagentoSettingsFactory(
        missing_product=ProductFactory(slug="missing"),
        shipping_product=ProductFactory(slug="shipping"),
    )
    magento_site = MagentoSiteFactory()
    MagentoSiteCountryFactory(
        magento_site=magento_site,
        magento_country_id="44",
        country=CountryFactory(iso2_code="FR"),
    )
    UserFactory(username="system.generated")
    with mock.patch(
        "magento.service.MagentoClient.credit_memo_list_filters"
    ) as mock_credit_memo_list_filters, mock.patch(
        "magento.service.MagentoClient.credit_memo"
    ) as mock_credit_memo, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_credit_memo_list_filters.return_value = [
            {
                "created_at": "2017-12-08",
                "increment_id": 134985,
                "invoice_id": 0,
            }
        ]
        mock_credit_memo.return_value = {
            "adjustment": "0.0000",
            "adjustment_negative": None,
            "adjustment_positive": None,
            "base_adjustment": "0.0000",
            "base_adjustment_negative": None,
            "base_adjustment_positive": None,
            "base_currency_code": "GBP",
            "base_discount_amount": "0.0000",
            "base_grand_total": "37.7300",
            "base_hidden_tax_amount": "0.0000",
            "base_shipping_amount": "-5.0100",
            "base_shipping_hidden_tax_amnt": None,
            "base_shipping_incl_tax": "-6.0100",
            "base_shipping_tax_amount": "-1.0000",
            "base_subtotal": "36.4500",
            "base_subtotal_incl_tax": "43.7400",
            "base_tax_amount": "6.2900",
            "base_to_global_rate": "1.0000",
            "base_to_order_rate": "1.0000",
            "billing_address_id": "78100",
            "created_at": "2019-05-03 11:41:05",
            "creditmemo_id": "1975",
            "creditmemo_status": None,
            "discount_amount": "0.0000",
            "discount_description": None,
            "email_sent": None,
            "global_currency_code": "GBP",
            "grand_total": "37.7300",
            "hidden_tax_amount": "0.0000",
            "increment_id": "100001920",
            "invoice_id": None,
            "order_currency_code": "GBP",
            "order_id": "39051",
            "order_increment_id": "100040370",
            "shipping_address_id": "78101",
            "shipping_amount": "-5.0100",
            "shipping_hidden_tax_amount": None,
            "shipping_incl_tax": "-6.0100",
            "shipping_tax_amount": "-1.0000",
            "state": "2",
            "store_currency_code": "GBP",
            "store_id": "1",
            "store_to_base_rate": "1.0000",
            "store_to_order_rate": "1.0000",
            "subtotal": "36.4500",
            "subtotal_incl_tax": "43.7400",
            "tax_amount": "6.2900",
            "transaction_id": None,
            "updated_at": "2019-05-03 11:41:05",
            "items": [
                {
                    "parent_id": "1975",
                    "base_price": "6.8000",
                    "tax_amount": "6.8000",
                    "base_row_total": "34.0000",
                    "discount_amount": None,
                    "row_total": "34.0000",
                    "base_discount_amount": None,
                    "price_incl_tax": "8.1600",
                    "base_tax_amount": "6.8000",
                    "base_price_incl_tax": "8.1600",
                    "qty": "-5.0000",
                    "base_cost": None,
                    "price": "6.8000",
                    "base_row_total_incl_tax": "40.8000",
                    "row_total_incl_tax": "40.8000",
                    "product_id": "24970",
                    "order_item_id": "437366",
                    "additional_data": None,
                    "description": None,
                    "sku": "mnproger_1Kg_on_Crystal_Organic_base_GBP6_80",
                    "name": "Rose Geranium Melt & Pour Soap",
                    "hidden_tax_amount": "0.0000",
                    "base_hidden_tax_amount": "0.0000",
                    "weee_tax_disposition": "0.0000",
                    "weee_tax_row_disposition": "0.0000",
                    "base_weee_tax_disposition": "0.0000",
                    "base_weee_tax_row_disposition": "0.0000",
                    "weee_tax_applied": "a:0:{}",
                    "base_weee_tax_applied_amount": "0.0000",
                    "base_weee_tax_applied_row_amnt": "0.0000",
                    "base_weee_tax_applied_row_amount": "0.0000",
                    "weee_tax_applied_amount": "0.0000",
                    "weee_tax_applied_row_amount": "0.0000",
                    "item_id": "4665",
                },
                {
                    "parent_id": "1975",
                    "base_price": "0.0000",
                    "tax_amount": None,
                    "base_row_total": None,
                    "discount_amount": None,
                    "row_total": None,
                    "base_discount_amount": None,
                    "price_incl_tax": None,
                    "base_tax_amount": None,
                    "base_price_incl_tax": None,
                    "qty": "-1.0000",
                    "base_cost": None,
                    "price": "0.0000",
                    "base_row_total_incl_tax": None,
                    "row_total_incl_tax": None,
                    "product_id": "24969",
                    "order_item_id": "437367",
                    "additional_data": None,
                    "description": None,
                    "sku": "mnproger_1Kg_on_Crystal_Organic_base_GBP6_80",
                    "name": "Mnproger 1Kg on Crystal Organic base GBP6.80",
                    "hidden_tax_amount": None,
                    "base_hidden_tax_amount": None,
                    "weee_tax_disposition": "0.0000",
                    "weee_tax_row_disposition": "0.0000",
                    "base_weee_tax_disposition": "0.0000",
                    "base_weee_tax_row_disposition": "0.0000",
                    "weee_tax_applied": "a:0:{}",
                    "base_weee_tax_applied_amount": "0.0000",
                    "base_weee_tax_applied_row_amnt": None,
                    "base_weee_tax_applied_row_amount": None,
                    "weee_tax_applied_amount": "0.0000",
                    "weee_tax_applied_row_amount": "0.0000",
                    "item_id": "4666",
                },
            ],
            "comments": [],
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "44",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": "EX20",
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_id": None,
            "customer_taxvat": "ABC",
            "increment_id": "100040370",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        assert 0 == Invoice.objects.filter(is_credit=True).count()
        assert 0 == InvoiceIssue.objects.count()
        download_credit_notes(magento_site, date(2018, 4, 23))
        qs = Invoice.objects.filter(is_credit=True)
        assert 1 == qs.count()
        credit_note = qs.first()
        assert Decimal("37.73") == credit_note.gross
        assert credit_note.has_lines is True
        assert set(["shipping", "missing"]) == set(
            [x.product.slug for x in credit_note.invoiceline_set.all()]
        )
        assert 1 == InvoiceIssue.objects.count()
        invoice_issue = InvoiceIssue.objects.first()
        assert "C100001920" == invoice_issue.invoice.invoice_number
        assert [
            "Not linked to an invoice",
            "Line totals adjusted to balance",
        ] == invoice_issue.lines()
