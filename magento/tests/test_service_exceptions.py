# -*- encoding: utf-8 -*-
import pytest

from unittest import mock

from magento.service import MagentoClient
from .factories import MagentoSiteFactory


@pytest.mark.parametrize(
    "price,net",
    [
        ("326.3362", "326.3400"),
        ("213.2931", "213.2900"),
        ("112.3103", "112.3100"),
    ],
)
@pytest.mark.django_db
def test_invoice_id_data(price, net):
    """Rounding issues with "price".

    https://www.kbsoftware.co.uk/crm/ticket/4812/

    """
    with mock.patch(
        "magento.service.MagentoClient.invoice_id"
    ) as mock_invoice_id, mock.patch(
        "magento.service.MagentoClient.sales_order"
    ) as mock_sales_order:
        mock_invoice_id.return_value = {
            "base_currency_code": "GBP",
            "base_discount_amount": "0.0000",
            "base_grand_total": "18.8800",
            "base_hidden_tax_amount": "0.0000",
            "base_shipping_amount": "4.4900",
            "base_shipping_hidden_tax_amnt": None,
            "base_shipping_incl_tax": "4.4900",
            "base_shipping_tax_amount": "0.0000",
            "base_subtotal": "11.9900",
            "base_subtotal_incl_tax": "14.3900",
            "base_tax_amount": "2.4000",
            "base_to_global_rate": "1.0000",
            "base_to_order_rate": "1.0000",
            "base_total_refunded": None,
            "billing_address_id": "27",
            "can_void_flag": None,
            "comments": [],
            "created_at": "2018-01-18 13:40:04",
            "discount_amount": "0.0000",
            "discount_description": None,
            "email_sent": "1",
            "global_currency_code": "GBP",
            "grand_total": "18.8800",
            "hidden_tax_amount": "0.0000",
            "increment_id": "100000003",
            "invoice_id": "3",
            "is_used_for_refund": None,
            "items": [
                {
                    "additional_data": None,
                    "base_cost": None,
                    "base_discount_amount": None,
                    "base_hidden_tax_amount": "0.0000",
                    "base_price": price,
                    "base_price_incl_tax": "14.3900",
                    "base_row_total": net,
                    "base_row_total_incl_tax": "14.3900",
                    "base_tax_amount": "2.4000",
                    "base_weee_tax_applied_amount": "0.0000",
                    "base_weee_tax_applied_row_amnt": "0.0000",
                    "base_weee_tax_applied_row_amount": "0.0000",
                    "base_weee_tax_disposition": "0.0000",
                    "base_weee_tax_row_disposition": "0.0000",
                    "description": None,
                    "discount_amount": None,
                    "hidden_tax_amount": "0.0000",
                    "item_id": "3",
                    "name": "Zenicolor 5",
                    "order_item_id": "20",
                    "parent_id": "3",
                    "price": "11.9900",
                    "price_incl_tax": "14.3900",
                    "product_id": "22488",
                    "qty": "1.0000",
                    "row_total": "11.9900",
                    "row_total_incl_tax": "14.3900",
                    "sku": "zeni5",
                    "tax_amount": "2.4000",
                    "weee_tax_applied": "a:0:{}",
                    "weee_tax_applied_amount": "0.0000",
                    "weee_tax_applied_row_amount": "0.0000",
                    "weee_tax_disposition": "0.0000",
                    "weee_tax_row_disposition": "0.0000",
                }
            ],
            "order_currency_code": "GBP",
            "order_id": "14",
            "order_increment_id": "100019917",
            "shipping_address_id": "28",
            "shipping_amount": "4.4900",
            "shipping_hidden_tax_amount": "0.0000",
            "shipping_incl_tax": "4.4900",
            "shipping_tax_amount": "0.0000",
            "state": "2",
            "store_currency_code": "GBP",
            "store_id": "1",
            "store_to_base_rate": "1.0000",
            "store_to_order_rate": "1.0000",
            "subtotal": "11.9900",
            "subtotal_incl_tax": "14.3900",
            "tax_amount": "2.4000",
            "total_qty": "1.0000",
            "transaction_id": "61a81513-55fc-e711-8640-00505691615b",
            "updated_at": "2018-01-18 13:40:04",
        }
        mock_sales_order.return_value = {
            "billing_address": {
                "city": "Oke",
                "company": "KB",
                "country_id": "22",
                "customer_address_id": "234",
                "firstname": "Pat",
                "lastname": "Kimber",
                "postcode": None,
                "region": "Devon",
                "street": "High Street",
                "telephone": "00123",
            },
            "customer_email": "test@test.com",
            "customer_taxvat": "ABC",
            "increment_id": "100037005",
            "order_id": 5555,
            "original_increment_id": 7777,
            "payment": {"method": "my-method"},
            "total_invoiced": 23,
            "total_paid": 23,
        }
        MagentoClient(MagentoSiteFactory()).invoice_id_data(100000003)
