# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from magento.models import MagentoSettings
from magento.tests.factories import MagentoSiteCustomerGroupFactory
from stock.tests.factories import ProductFactory
from .factories import MagentoSiteFactory, MagentoSitePaymentMethodFactory


@pytest.mark.django_db
def test_magento_settings_update(client):
    product_1 = ProductFactory()
    product_2 = ProductFactory()
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("magento.settings.update")
    response = client.post(
        url,
        data={
            "missing_product": product_1.pk,
            "shipping_product": product_2.pk,
        },
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("project.settings") == response.url
    assert 1 == MagentoSettings.objects.count()
    magento_settings = MagentoSettings.load()
    assert product_1 == magento_settings.missing_product
    assert product_2 == magento_settings.shipping_product


@pytest.mark.django_db
def test_magento_settings_update_missing(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("magento.settings.update")
    response = client.post(url, data={"days_to_sync": 5})
    # check
    assert HTTPStatus.OK == response.status_code, response.context[
        "form"
    ].errors
    form = response.context["form"]
    assert {
        "__all__": [
            "Please select a product for shipping and a "
            "product for transactions with missing lines"
        ]
    } == form.errors


@pytest.mark.django_db
def test_magento_site_create(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("magento.site.create"),
        data={
            "description": "Pound Sterling",
            "api_url": "http://localhost",
            "username": "patrick",
            "password": "pass",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("magento.site.list") == response.url


@pytest.mark.django_db
def test_magento_payment_method_list(client):
    magento_site = MagentoSiteFactory()
    MagentoSitePaymentMethodFactory(slug="a", magento_site=magento_site)
    MagentoSitePaymentMethodFactory(slug="b", magento_site=MagentoSiteFactory())
    MagentoSitePaymentMethodFactory(slug="c", magento_site=magento_site)
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("magento.site.payment.method.list", args=[magento_site.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "magento_site" in response.context
    assert magento_site == response.context["magento_site"]
    assert "object_list" in response.context
    qs = response.context["object_list"]
    assert ["a", "c"] == [x.slug for x in qs]


@pytest.mark.django_db
def test_magento_site_get(client):
    magento_site = MagentoSiteFactory()
    group_1 = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=44
    )
    group_2 = MagentoSiteCustomerGroupFactory(
        magento_site=MagentoSiteFactory(), magento_customer_group_id=45
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=46
    )
    group_4 = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=47
    )
    magento_site.on_account_customer_groups.add(group_4)
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("magento.site.update", args=[magento_site.pk]),
    )
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert [group_4] == form.initial["on_account_customer_groups"]
    on_account_customer_groups = form.fields["on_account_customer_groups"]
    assert [44, 46, 47] == [
        x.magento_customer_group_id for x in on_account_customer_groups.queryset
    ]


@pytest.mark.django_db
def test_magento_site_update(client):
    magento_site = MagentoSiteFactory()
    customer_group_1 = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=7, code="Apple"
    )
    customer_group_2 = MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=4, code="Orange"
    )
    MagentoSiteCustomerGroupFactory(
        magento_site=magento_site, magento_customer_group_id=3, code="Pear"
    )
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("magento.site.update", args=[magento_site.pk]),
        data={
            "description": "Pound Sterling",
            "api_url": "http://localhost",
            "username": "patrick",
            "password": "pass",
            "on_account_customer_groups": [
                customer_group_1.pk,
                customer_group_2.pk,
            ],
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("magento.site.list") == response.url
    magento_site.refresh_from_db()
    assert "Pound Sterling" == magento_site.description
    assert "http://localhost" == magento_site.api_url
    assert "patrick" == magento_site.username
    assert "pass" == magento_site.password
    assert [customer_group_2.pk, customer_group_1.pk] == [
        x.pk for x in magento_site.on_account_customer_groups.all()
    ]
