# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from unittest import mock

from login.tests.fixture import perm_check
from .factories import MagentoSitePaymentMethodFactory, MagentoSiteFactory


@pytest.mark.django_db
def test_credit_detail(perm_check):
    MagentoSiteFactory()
    with mock.patch("magento.models.MagentoClient.credit_memo") as m:
        m.return_value = {
            "grand_total": 1,
            "increment_id": 222,
            "invoice_id": 2,
            "items": [],
            "order_id": 3,
            "order_increment_id": 4,
            "subtotal": 5,
            "tax_amount": 6,
        }
        perm_check.superuser(
            reverse("magento.credit.detail", args=["a1b2c3d4e5"])
        )


@pytest.mark.django_db
def test_customer_detail(perm_check):
    MagentoSiteFactory()
    with mock.patch("magento.service.MagentoClient.customer") as m:
        m.return_value = {
            "company_name": "KB",
            "customer_id": 4545,
            "email": "patrick@kbsoftware.co.uk",
            "firstname": "Patrick",
            "group_id": 4646,
            "increment_id": 4545,
            "lastname": "Kimber",
        }
        perm_check.superuser(reverse("magento.customer.detail", args=[4545]))


@pytest.mark.django_db
def test_invoice_detail(perm_check):
    MagentoSiteFactory()
    with mock.patch("magento.models.MagentoClient.invoice") as m:
        m.return_value = {
            "grand_total": 1,
            "increment_id": 222,
            "invoice_id": 2,
            "items": [],
            "order_id": 3,
            "order_increment_id": 4,
            "subtotal": 5,
            "tax_amount": 6,
        }
        perm_check.superuser(
            reverse("magento.invoice.detail", args=["a1b2c3d4e5"])
        )


@pytest.mark.django_db
def test_sales_order_detail(perm_check):
    MagentoSiteFactory()
    with mock.patch("magento.models.MagentoClient.sales_order") as m:
        m.return_value = {
            "base_grand_total": 1,
            "base_tax_amount": 2,
            "customer_email": 3,
            "customer_firstname": 4,
            "customer_lastname": 5,
            "items": [],
            "increment_id": 555,
            "order_id": 6,
            "payment": {"amount_paid": 7},
            "sales_order_detail": 8,
        }
        perm_check.superuser(
            reverse("magento.sales.order.detail", args=["a1b2c3d4e5"])
        )


@pytest.mark.django_db
def test_magento_payment_method_list(perm_check):
    x = MagentoSitePaymentMethodFactory()
    url = reverse("magento.site.payment.method.list", args=[x.magento_site.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_magento_payment_method_update(perm_check):
    x = MagentoSitePaymentMethodFactory()
    url = reverse("magento.site.payment.method.update", args=[x.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_magento_settings_update(perm_check):
    url = reverse("magento.settings.update")
    perm_check.staff(url)


@pytest.mark.django_db
def test_magento_site_create(perm_check):
    url = reverse("magento.site.create")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_magento_site_audit_list(perm_check):
    magento_site = MagentoSiteFactory()
    url = reverse("magento.site.audit.list", args=[magento_site.pk])
    perm_check.superuser(url)


@pytest.mark.django_db
def test_magento_site_list(perm_check):
    url = reverse("magento.site.list")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_magento_site_update(perm_check):
    magento_site = MagentoSiteFactory()
    url = reverse("magento.site.update", args=[magento_site.pk])
    perm_check.superuser(url)
