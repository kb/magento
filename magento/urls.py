# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import (
    CreditTemplateView,
    CustomerTemplateView,
    InvoiceTemplateView,
    MagentoSettingsUpdateView,
    MagentoSiteAuditListView,
    MagentoSiteCreateView,
    MagentoSiteListView,
    MagentoSitePaymentMethodListView,
    MagentoSitePaymentMethodUpdateView,
    MagentoSiteUpdateView,
    SalesOrderTemplateView,
)


urlpatterns = [
    url(
        regex=r"^credit/(?P<slug>[-\w\d]+)/$",
        view=CreditTemplateView.as_view(),
        name="magento.credit.detail",
    ),
    url(
        regex=r"^customer/(?P<customer_id>\d+)/$",
        view=CustomerTemplateView.as_view(),
        name="magento.customer.detail",
    ),
    url(
        regex=r"^invoice/(?P<slug>[-\w\d]+)/$",
        view=InvoiceTemplateView.as_view(),
        name="magento.invoice.detail",
    ),
    url(
        regex=r"^sales/order/(?P<slug>[-\w\d]+)/$",
        view=SalesOrderTemplateView.as_view(),
        name="magento.sales.order.detail",
    ),
    url(
        regex=r"^settings/update/$",
        view=MagentoSettingsUpdateView.as_view(),
        name="magento.settings.update",
    ),
    url(
        regex=r"^site/(?P<magento_site_pk>\d+)/audit/$",
        view=MagentoSiteAuditListView.as_view(),
        name="magento.site.audit.list",
    ),
    url(
        regex=r"^site/create/$",
        view=MagentoSiteCreateView.as_view(),
        name="magento.site.create",
    ),
    url(
        regex=r"^site/$",
        view=MagentoSiteListView.as_view(),
        name="magento.site.list",
    ),
    url(
        regex=r"^site/(?P<magento_site_pk>\d+)/payment/method/$",
        view=MagentoSitePaymentMethodListView.as_view(),
        name="magento.site.payment.method.list",
    ),
    url(
        regex=r"^site/payment/method/(?P<pk>\d+)/$",
        view=MagentoSitePaymentMethodUpdateView.as_view(),
        name="magento.site.payment.method.update",
    ),
    url(
        regex=r"^site/(?P<pk>\d+)/update/$",
        view=MagentoSiteUpdateView.as_view(),
        name="magento.site.update",
    ),
]
