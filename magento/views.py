# -*- encoding: utf-8 -*-
import json

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.core.exceptions import ImproperlyConfigured
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse
from django.views.generic import CreateView, ListView, TemplateView, UpdateView

from base.view_utils import BaseMixin
from .models import (
    MagentoError,
    MagentoSettings,
    MagentoSite,
    MagentoSiteAudit,
    MagentoSitePaymentMethod,
)
from .forms import (
    MagentoSettingsForm,
    MagentoSiteForm,
    MagentoSitePaymentMethodForm,
)
from .service import MagentoClient


class InvoiceCreditMixin:
    """Display the Magento data for an invoice or credit note."""

    template_name = "magento/invoice_detail.html"

    def _caption(self, increment_id):
        """Return the caption / title for the template."""
        raise ImproperlyConfigured(
            "{} is missing a '_caption' method.".format(self.__class__.__name__)
        )

    def _data(self, increment_id):
        """Return the Magento API data for the required object."""
        raise ImproperlyConfigured(
            "{} is missing a '_data' method.".format(self.__class__.__name__)
        )

    def _increment_id(self):
        return self.kwargs.get("slug")

    def _magento_site(self):
        """Find the Magento site."""
        if 1 == MagentoSite.objects.count():
            return MagentoSite.objects.first()
        else:
            raise MagentoError(
                "This view will only work when we have a single Magento site"
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = json_data = None
        increment_id = self._increment_id()
        if increment_id:
            data = self._data(increment_id)
            # self.write_to_file(data)
            json_data = json.dumps(data, indent=4, cls=DjangoJSONEncoder)
        context.update(
            dict(
                caption=self._caption,
                data=data,
                increment_id=increment_id,
                json_data=json_data,
            )
        )
        return context

    def write_to_file(self, data):
        """PJK 21/06/2019 Comment out for now ref writing on the server."""
        with open("invoice.json", "w") as f:
            f.write(json.dumps(data, indent=4))


class CreditTemplateView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    InvoiceCreditMixin,
    BaseMixin,
    TemplateView,
):
    def _caption(self):
        return "Credit Note"

    def _data(self, increment_id):
        magento = MagentoClient(self._magento_site())
        return magento.credit_memo({"increment_id": increment_id})


class CustomerTemplateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "magento/customer_detail.html"

    def _magento_site(self):
        """Find the Magento site.

        .. note:: This is a temporary method until we know if we even need the
                  ``MagentoSite`` model.
                  If we do need the ``MagentoSite`` model, then we will need to
                  include the ``site_pk`` in the URL for this view.

        """
        magento_site = MagentoSite.objects.first()
        if not magento_site:
            raise MagentoError("The system has no Magento sites")
        return magento_site

    def _customer_id(self):
        return self.kwargs.get("customer_id")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        customer_id = self._customer_id()
        if customer_id:
            magento = MagentoClient(self._magento_site())
            data = magento.customer(customer_id)
            # self.write_to_file(data)
            json_data = json.dumps(data, indent=4, cls=DjangoJSONEncoder)
        context.update(
            dict(data=data, customer_id=customer_id, json_data=json_data)
        )
        return context

    def write_to_file(self, data):
        """PJK 21/06/2019 Comment out for now ref writing on the server."""
        with open("sales_order.json", "w") as f:
            f.write(json.dumps(data, indent=4))


class InvoiceTemplateView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    InvoiceCreditMixin,
    BaseMixin,
    TemplateView,
):
    def _caption(self):
        return "Invoice"

    def _data(self, increment_id):
        magento = MagentoClient(self._magento_site())
        return magento.invoice({"increment_id": increment_id})


class SalesOrderTemplateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "magento/sales_order_detail.html"

    def _magento_site(self):
        """Find the Magento site.

        .. note:: This is a temporary method until we know if we even need the
                  ``MagentoSite`` model.
                  If we do need the ``MagentoSite`` model, then we will need to
                  include the ``site_pk`` in the URL for this view.

        """
        magento_site = MagentoSite.objects.first()
        if not magento_site:
            raise MagentoError("The system has no Magento sites")
        return magento_site

    def _slug(self):
        return self.kwargs.get("slug")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        slug = self._slug()
        if slug:
            magento = MagentoClient(self._magento_site())
            data = magento.sales_order({"increment_id": slug})
            # self.write_to_file(data)
            json_data = json.dumps(data, indent=4, cls=DjangoJSONEncoder)
        context.update(dict(data=data, slug=slug, json_data=json_data))
        return context

    def write_to_file(self, data):
        """PJK 21/06/2019 Comment out for now ref writing on the server."""
        with open("sales_order.json", "w") as f:
            f.write(json.dumps(data, indent=4))


class MagentoSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = MagentoSettingsForm

    def get_object(self):
        return MagentoSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class MagentoSiteAuditListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):

    model = MagentoSiteAudit

    def _magento_site(self):
        magento_site_pk = self.kwargs["magento_site_pk"]
        return MagentoSite.objects.get(pk=magento_site_pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(magento_site=self._magento_site()))
        return context

    def get_queryset(self):
        magento_site = self._magento_site()
        return magento_site.audit().order_by("-pk")


class MagentoSiteCreateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, CreateView
):
    form_class = MagentoSiteForm
    model = MagentoSite

    def get_success_url(self):
        return reverse("magento.site.list")


class MagentoSiteListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):

    model = MagentoSite


class MagentoSitePaymentMethodListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):

    model = MagentoSitePaymentMethod

    def _magento_site(self):
        magento_site_pk = self.kwargs["magento_site_pk"]
        return MagentoSite.objects.get(pk=magento_site_pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(magento_site=self._magento_site()))
        return context

    def get_queryset(self):
        magento_site = self._magento_site()
        return MagentoSitePaymentMethod.objects.current(magento_site).order_by(
            "slug"
        )


class MagentoSitePaymentMethodUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = MagentoSitePaymentMethodForm
    model = MagentoSitePaymentMethod

    def get_success_url(self):
        return reverse(
            "magento.site.payment.method.list",
            args=[self.object.magento_site.pk],
        )


class MagentoSiteUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = MagentoSiteForm
    model = MagentoSite

    def get_success_url(self):
        return reverse("magento.site.list")
