-r base.txt
-e .
-e ../base
-e ../chat
-e ../contact
-e ../crm
-e ../finance
-e ../gallery
-e ../gdpr
-e ../googl
-e ../invoice
-e ../login
-e ../mail
-e ../stock
black
django-debug-toolbar
factory-boy
freezegun
GitPython
psycopg2
pytest
pytest-cov
pytest-django
pytest-flakes
PyYAML
responses
rich
semantic-version
walkdir
